/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: strChange.h
 *
 * About:
 *
 * Support for string handler C functions.
 */

#ifndef STRCHANGE_H_
#define STRCHANGE_H_

extern int strPingIt(unsigned char writable, char *newVal);
extern int strAdminIP(unsigned char writable, char *newVal);
extern int strAdminMask(unsigned char writable, char *newVal);
extern int strNetworkIP(unsigned char writable, char *newVal);
extern int strNetworkMask(unsigned char writable, char *newVal);
extern int strWirelessIP(unsigned char writable, char *newVal);
extern int strWirelessMask(unsigned char writable, char *newVal);
extern int strNetworkGateway(unsigned char writable, char *newVal);

#endif /* STRCHANGE_H_ */
