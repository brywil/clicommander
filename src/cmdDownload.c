/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cmdDownload.c
 *
 * About:
 *
 * This file contains functionality of the OC's download/codeload capability.
 */

#include "common.h"
#include "exCmd.h"
#include "e2Change.h"
#include "cmdOptions.h"
#include <curl/curl.h>
#include <math.h>
#include <sys/types.h>
#include <sys/wait.h>

#define CHKSPEED_VERSION "1.0"

static int parseManifest(char *devName, char *partNum, char *verNum, char *custCode, char *imgDate);
static int downloadSoftware(char *devName, char *partNum, char *verNum, char *custCode);
static int progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded);
static int curlFileXfer(FILE *fp, char *curlurl, int showstat);

/******************************************************
 * Function	   : cmdSoftwareUpdate
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Searches LynxSpring server for new version
 * of software.  Prompts the user if update should continue
 * if new software.  Downloads software using DD, overwriting
 * the "restore image" software on the boot partition.
 ******************************************************/
int cmdSoftwareUpdate(void)
{
	int retVal = CMD_ERROR;
	char cmd[100];
	char devName[75];
	char partNum[75];
	char custCode[75];
	char verNum[25];
	char imgDate[64];
	int sw_major = 0;
	int sw_minor = 0;
	int new_major = 0;
	int new_minor = 0;

	memset(devName, 0, sizeof(devName));
	memset(partNum, 0, sizeof(partNum));
	memset(verNum, 0, sizeof(verNum));
	memset(imgDate, 0, sizeof(imgDate));

	cliPrint("\nDo you have a Customer Code?  If so, enter it here\n");
	cliPrint("otherwise press <RETURN>.\n");
	cliPrint("Code: ");
	memset(custCode, 0, sizeof(custCode));

	getInput(custCode, sizeof(custCode) - 1);

	cliPrint("\nChecking for updates...\n");

	if (parseManifest(devName, partNum, verNum, custCode, imgDate) == CMD_SUCCESS)
	{
		sscanf(verNum, "%d.%d", &new_major, &new_minor);
		getSoftwareVersion(&sw_major, &sw_minor);

		if (((new_major > sw_major) || (new_major == sw_major && new_minor > sw_minor)) || strlen(custCode))
		{
			/* New version available.  Download! */

			cliPrint("\n\n***************************************************\n");
			cliPrint("* WARNING WARNING WARNING WARNING WARNING WARNING *\n");
			cliPrint("*                                                 *\n");
			cliPrint("* Software update available.  Continuing this     *\n");
			cliPrint("* operation may result in lost custom information.*\n");
			cliPrint("*                                                 *\n");
			cliPrint("* This operation can take ~30 mins to complete    *\n");
			cliPrint("* and should not be interrupted once started!     *\n");
			cliPrint("*                                                 *\n");
			cliPrint("* WARNING WARNING WARNING WARNING WARNING WARNING *\n");
			cliPrint("***************************************************\n");

			cliPrint("\nCurrent software version: %d.%d\n", sw_major, sw_minor);
			cliPrint("New software version: %d.%d\n\n", new_major, new_minor);

			cliPrint("Download new software? (y/n): ");
			memset(cmd, 0, sizeof(cmd));

			if (getInput(cmd, 2))
			{
				if (cmd[0] == 'y' || cmd[0] == 'Y')
				{
					retVal = downloadSoftware(devName, partNum, verNum, custCode);
				} else {
					cliPrint("\nOperation aborted.\n");
				}
			} else {
				cliPrint("\nOperation aborted.\n");
			}
		} else {
			cliPrint("\n\nYou are already running the latest version.\n");
		}
	} else {
		if (strlen(custCode))
		{
			cliPrint("\nCould not find version indicated by Customer Code.  Contact customer service.\n");
		} else {
			cliPrint("\nCould not negotiate with server.  Try again later or contact customer service.");
		}
	}

	return retVal;
}

/******************************************************
 * Function		: downloadSoftware
 * Input		: char * name of device
 *                char * part number
 *                char * Sub Version for One-offs.
 *                char * version number "major.minor"
 * Output		: CMD_SUCCESS or CMD_ERROR
 *
 * Description	: This function attempts to download a new
 * image from the LynxSpring image server.  Once downloaded,
 * the image is deployed by "faking" the system into
 * think it is doing a "restore device image" operation.
 ******************************************************/
int downloadSoftware(char *devName, char *partNum, char *verNum, char *custCode)
{
	int retVal = CMD_ERROR;
	char cmd[255];
	char destImage[255];
	FILE *fp = NULL;

	if (devName && partNum)
	{
		if (getImageRoot(destImage) == CMD_SUCCESS)
		{
			cliPrint("\nDownloading, please wait...\n");

			/* Open output file */

			sprintf(cmd, "%sboot/%s", destImage, FACTORY_IMAGE_FILE);
			if ((fp = fopen(cmd, "w")) != NULL)
			{
				sprintf(cmd, "ftp://%s/ftp/images/%s/%s/%s.img", LYNXSPRING_FILESERVER_IP, devName, partNum, (strlen(custCode) != 0) ? custCode : verNum);
				if (curlFileXfer(fp, cmd, TRUE) == CURLE_OK)
					retVal = CMD_SUCCESS;
			}

			if (fp)
				fclose(fp);


			if (retVal == CMD_SUCCESS)
				retVal = factoryImageReset();

		} else {
			cliPrint("\nFile system error.  Reboot and try again.\n");
		}
	}

	return retVal;
}

/******************************************************
 * Function		: parseManifest
 * Input		: char * name of device
 *                char * part number
 *                char * Version
 *                char * customer code
 *                char * file date
 * Output		: CMD_SUCCESS or CMD_ERROR
 *
 * Description	: This function attempts to download a new
 * image from the LynxSpring image server.  Once downloaded,
 * the image is deployed by "faking" the system into
 * think it is doing a "restore device image" operation.
 ******************************************************/
static int parseManifest(char *devName, char *partNum, char *verNum, char *custCode, char *imgDate)
{
	int retVal = CMD_ERROR;
	char cmd[255];
	char cc[40];
	int len;
	char *token = NULL;
	char *newline = NULL;
	char seps[] = ",";
	int pos;
	size_t nsize = 0;
	FILE *fp = NULL;

	/* Transfer manifest from LynxSpring server by cat'ing the contents of the CSV file to our resulting buffer
	 * for parsing.
	 */

	/* Delete the following 3 lines, used for simulation only */

	strcpy(onyxxConfig.board_name, "BBB");
	strcpy(onyxxConfig.part_number, "CE-121");
	onyxxConfig.version = 0;

	if ((fp = fopen(MANIFEST_FILE, "w+")) != NULL)
	{
		sprintf(cmd, "ftp://%s/LynxSpring_Manifest.csv",LYNXSPRING_FILESERVER_IP);
		if (curlFileXfer(fp, cmd, FALSE) == CURLE_OK)
		{
			fclose(fp);

			if ((fp = fopen(MANIFEST_FILE, "r")) != NULL)
			{
				/* Parse manifest file line by line. */

				while (!(feof(fp)) && retVal != CMD_SUCCESS)
				{
					newline = NULL;
					if ((len = getline(&newline, &nsize, fp)) != 0)
					{
						/* For each line, look for format of:
						 * board name, part number, version and release date.
						 */

						if (len && len < sizeof(cmd))
						{
							/* Pull data out of the line:
							 * board name, part number, version, special code and release date.
							 */

							token = strtok(newline, seps);
							pos = 1;
							while (token != NULL)
							{
								switch(pos++) {
									case 1: // Board Name
										sscanf(token, "%s", devName);
										break;
									case 2: // Part Number
										sscanf(token, "%s", partNum);
										break;
									case 3: // Version
										sscanf(token, "%s", verNum);
										break;
									case 4: // Release Date
										sscanf(token, "%s", imgDate);
										break;
									case 5: // Special Code
										sscanf(token, "%s", cc);
										break;
									case 6: // Notes
										/* Notes can have commas in them but are surrounded by quote marks. */
										token = NULL;
										break;
									default:
										break;
								}

								if (token)
									token = strtok(NULL, seps);
							}

							/* Versions mean nothing if we're using a customer code.  If we're using a customer code, find the entry
							 * that matches that code.
							 */

							if (stricmp(onyxxConfig.board_name, devName) == 0 && stricmp(onyxxConfig.part_number, partNum) == 0)
							{
								if (strlen(custCode))
								{
									if (stricmp(custCode, cc) == 0)
										retVal = CMD_SUCCESS;
								} else {
									retVal = CMD_SUCCESS;
								}
							}

							if (newline)
								free(newline);
						}
					}
				}
			} else {
				cliPrint("ERR: File system, reboot and try again.\n");
			}
		}
	}

	if (fp)
		fclose(fp);

	/* Delete the manifest file if it was created. */

	sprintf(cmd, "%s", MANIFEST_FILE);
	remove(cmd);

	return retVal;
}

/******************************************************
 * Function		: getImageRoot
 * Input		: char * returned root path
 * Output		: CMD_SUCCESS or CMD_ERROR
 *
 * Description	: This function returns a string pointed to
 * the factory path.  The path will be:
 *  /       When booted from factory partition
 *  /mnt/p1 When booted from application partition
 * If application partition is in the mnt/p1 directory,
 * then it is automatically mounted to the mnt/p1 directory.
 ******************************************************/
int getImageRoot(char *dest)
{
	int retVal = CMD_ERROR;
	char cmd[255];

	if (dest)
	{
		dest[0] = 0;

		/* Which partition are we on, boot or app? */

		sprintf(cmd, "/boot/%s", FACTORY_PART_IMAGE_FILE);

		if (access(cmd, F_OK) == 0)
		{
			/* We are on the boot partition, not the app.   Therefore, the restoration
			 * image should be at /boot
			 */

			sprintf(dest, "/");
			retVal = CMD_SUCCESS;
		} else {
			sprintf(cmd, "/boot/%s", APP_PART_IMAGE_FILE);

			/* Not on the boot partition, are we on the app partition? */

			if (access(cmd, F_OK) == 0)
			{
				/* On app partition.  Restoration image always on boot partition.
				 * We need to mount the boot partition. */

				sprintf(cmd, "mount /dev/mmcblk0p1 %s 2>&1", MOUNT_DIR_P1);
				if (executeCommand(cmd, resultBuf) == CMD_SUCCESS)
				{
					/* If partition mounted  correctly, we should find the
					 * boot part image file on the mnt drive. */

					sprintf(cmd, "%sboot/%s", MOUNT_DIR_P1, FACTORY_PART_IMAGE_FILE);
					if (access(cmd, F_OK) == 0)
					{
						/* Confirmed, boot parition is in mnt directory.  Point to it. */

						sprintf(dest, "%s", MOUNT_DIR_P1);
						retVal = CMD_SUCCESS;
					}
				}
			}
		}
	}

    return retVal;
}

/******************************************************
 * Function		: getSoftwareVersion
 * Input		: int * return of major #
 * 				  int * return of minor #
 * Output		: CMD_SUCCESS or CMD_ERROR
 *
 * Description	: Reads the software version file from
 * the application partition's boot directory and returns
 * two integers, major and minor numbers.
 ******************************************************/
int getSoftwareVersion(int *major, int *minor)
{
	int retVal = CMD_ERROR;
	FILE *fp = NULL;
	char cmd[255];
	char path[100];

	/* Read the software version file located at: SOFTWARE_VERSION_FILE */

	if (major && minor)
	{
		*major = 0;
		*minor = 0;

		/* Software version located in boot directory of application partition.  Make sure
		 * we pull it from that locale.
		 */

		if (getImageRoot(cmd) == CMD_SUCCESS)
		{
			if (strcmp(cmd, "/") == 0)
			{
				/* We are on the factory partition, need to mount and get access to the
				 * application partition.
				 */

				sprintf(cmd, "mount /dev/mmcblk0p2 %s 2>&1", MOUNT_DIR_P2);
				if (executeCommand(cmd, resultBuf) == CMD_SUCCESS)
				{
					sprintf(path, "%s%s", MOUNT_DIR_P2, SOFTWARE_VERSION_FILE);
				}
			} else {
				sprintf(path, "%s", SOFTWARE_VERSION_FILE);
			}

			if ((fp = fopen(path, "r")) != NULL)
			{
				fscanf(fp, "%d.%d", major, minor);
				retVal = CMD_SUCCESS;
			}

			if (fp)
				fclose(fp);
		}
	}

	return retVal;
}

/******************************************************
 * Function		: progress_func
 * Input		: CBM specified parameters
 * Output		: CBM specified parameters
 *
 * Description	: Helper CBM function for curlFileXfer.
 * Called by libcurl to indicate status of file xfer visually.
 ******************************************************/
int progress_func(void* ptr, double TotalToDownload, double NowDownloaded,
                    double TotalToUpload, double NowUploaded)
{
	int totaldotz = 40;
	static int lastval = 0;
	double fractiondownloaded = 0;
	int dotz = 0;
	int i;
	char outdata[255];

    // ensure that the file to be downloaded is not empty
    // because that would cause a division by zero error later on
    if (TotalToDownload > 0)
    {
		// how wide you want the progress meter to be

		fractiondownloaded = NowDownloaded / TotalToDownload;

		// part of the progressmeter that's already "full"

		dotz = fractiondownloaded * totaldotz;

		if (dotz != lastval)
		{
			lastval = dotz;

			// create the "meter"
			i=0;
			sprintf(outdata, "%3.0f%% [",fractiondownloaded * 100);
			// part  that's full already
			for ( ; i < dotz;i++) {
				strcat(outdata, "=");
			}
			// remaining part (spaces)
			for ( ; i < totaldotz;i++) {
				strcat(outdata, " ");
			}
			// and back to line begin - do not forget the fflush to avoid output buffering problems!
			strcat(outdata, "]\r");

			cliPrint(outdata);
		}
    }

    return 0;
}

/******************************************************
 * Function		: curlFileXfer
 * Input		: FILE * of output destination
 * 				  char * to download URL
 * 				  Bool stat: TRUE = show stats, !TRUE = hide
 * Output		: CURLcode status of xfer
 *
 * Description	: Uses Libcurl to download the file indicated
 * by the URL.  Downloaded file is stored in local FP.  To
 * avoid users from CTRL-C'ing curl, we fork it as its own
 * thread and wait for completion.  If the threat is interrupted
 * by the user, we can still continue without going to a
 * command prompt.
 ******************************************************/
pid_t childPID;
CURLcode res;

static int curlFileXfer(FILE *fp, char *curlurl, int stat)
{
	char cmd[255];
	CURL *curl_handle = NULL;
	double val;

	/* Init CURL */

	curl_global_init(CURL_GLOBAL_ALL);

	/* Init CURL session */

	curl_handle = curl_easy_init();

	/* Set output */

	curl_easy_setopt(curl_handle, CURLOPT_FILE, fp);

	/* Use our progress bar */

	curl_easy_setopt(curl_handle, CURLOPT_NOPROGRESS, FALSE);
	curl_easy_setopt(curl_handle, CURLOPT_PROGRESSFUNCTION, progress_func);

	/* Set curl URL */

	curl_easy_setopt(curl_handle, CURLOPT_URL, curlurl);

	/* Required for some servers */

	curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, CHKSPEED_VERSION);

	/* Timeout */

	curl_easy_setopt(curl_handle, CURLOPT_TIMEOUT, 1800);

	/* Access is to our anonymous onyxx account */

	sprintf(cmd, "onyxx:onyxx");
	curl_easy_setopt(curl_handle, CURLOPT_USERPWD, cmd);

	/* Get the file */

	childPID = fork();

	if (childPID >= 0)
	{
		if (childPID == 0)
		{
			res = curl_easy_perform(curl_handle);

			/* Display stats */

			cliPrint("\n\n");

			if (stat == TRUE)
			{
				if (CURLE_OK == res)
				{
					/* check for bytes downloaded */

					res = curl_easy_getinfo(curl_handle, CURLINFO_SIZE_DOWNLOAD, &val);
					if((CURLE_OK == res) && (val > 0))
						cliPrint("Data downloaded: %0.0f bytes.\n", val);

					/* check for total download time */
					res = curl_easy_getinfo(curl_handle, CURLINFO_TOTAL_TIME, &val);
					if((CURLE_OK == res) && (val > 0))
						cliPrint("Total download time: %0.3f sec.\n", val);

					/* check for average download speed */
					res = curl_easy_getinfo(curl_handle, CURLINFO_SPEED_DOWNLOAD, &val);
					if((CURLE_OK == res) && (val > 0))
						cliPrint("Average download speed: %0.3f kbyte/sec.\n", val / 1024);

					/* check for connect time */
					res = curl_easy_getinfo(curl_handle, CURLINFO_CONNECT_TIME, &val);
					if((CURLE_OK == res) && (val > 0))
						cliPrint("Connect time: %0.3f sec.\n", val);

					cliPrint("\n");
				} else {
					cliPrint("\nDownload failed: %s\n", curl_easy_strerror(res));
				}
			}

			// Clean up curl

			curl_easy_cleanup(curl_handle);
			curl_global_cleanup();
		} else {
			waitpid(childPID, NULL, 0);
		}
	}

	return res;
}
