/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: tglChange.h
 *
 * About:
 *
 * Support data for tglChange.c.
 */

#ifndef TGLCHANGE_H_
#define TGLCHANGE_H_

#include "common.h"

extern int toggleNetworking(unsigned char b);
extern int toggleWatchdog(unsigned char b);
extern int toggleRTC(unsigned char b);
extern int toggleWifi(unsigned char b);
extern int toggleNTP(unsigned char b);
extern int toggleOnyxx(unsigned char b);
extern int toggleMiniJack(unsigned char b);
extern int toggleLogView(unsigned char b);
extern int toggleLogLevel(unsigned char b);
extern int toggleLogType(unsigned char b);
extern int toggleInterface(unsigned char b);

#endif /* TGLCHANGE_H_ */
