/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: common.h
 *
 * About:
 *
 * Common support data for Onyxx-Commander and is used code-wide.
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <memory.h>
#include <mcrypt.h>
#include <stdint.h>
#include <stdbool.h>
#include <termios.h>
#include <stdarg.h>

#define true 1
#define false 0
#define TRUE 1
#define FALSE 0

#define FEATURE_UNKNOWN 	0
#define FEATURE_ENABLED 	1
#define FEATURE_DISABLED 	2
#define CMD_SUCCESS			0
#define CMD_ERROR			1

#define KYB_ESCAPE 0x1b
#define KYB_ENTER 0x0a

typedef int (*callback_int) (unsigned char);
typedef int (*callback_str) (unsigned char, char *);
typedef int (*callback_cmd) (void);

#define LYNXSPRING_FILESERVER_IP "10.2.1.125"

#define e2FILE 			"/sys/bus/i2c/devices/0-0050/at24-0/nvmem" /* This is a 4k-byte part */
#define ORIG_ROUTEV4 	"/opt/stats/.orig_routev4"
#define ORIG_ROUTEV6 	"/opt/stats/.orig_routev6"
#define ORIG_INTERFACE 	"/opt/stats/.orig_interface"
#define SOFTWARE_VERSION_FILE "/boot/.swversion"
#define LOG_FILE_DIR    "/var/log/"
#define LOG_FILE_CB     "/opt/stats/logs/"
#define FACTORY_IMAGE_FILE "factory.img.gz"
#define FACTORY_RESET_FILE "factory_reset"
#define FACTORY_PART_IMAGE_FILE "vmlinuz-factory-boot"
#define APP_PART_IMAGE_FILE "vmlinuz-app-boot"
#define MOUNT_DIR_P1 "/mnt/p1/"
#define MOUNT_DIR_P2 "/mnt/p2/"
#define MANIFEST_FILE ".manifest"

/* COFFEECAFE indicates a board has been previously initialized when
 * founded in E^2's sanity field. */

#define SANITY_STRING	"0xc0\0xff\0xee\0xca\0xfe"

#define CTL_MOD_START 0x44e10000
#define CTL_MOD_END   0x44e11fff
#define CTL_MOD_SIZE (CTL_MOD_END - CTL_MOD_START)

/* Size of EEPROM we're interested in is 4k.  The first 2k
 * is strictly reserved, we never want to touch it or we could brick the unit.
 */

#define EEPROM_SIZE (0xf00)		/* 3,840 bytes, from 0x0100 to 0xffff is all LSM property */
#define EEPROM_START_ADDR (0x100) 	/* Way past any important stuff already in eeprom, first 256-bytes reserved. */

/* Size of block to write to EEPROM.  Note we print a status string for each block
 * so don't make this number too small or it'll flood the terminal.
 */

#define EEPROM_BLK_SIZE (0x80) /* 128-byte block sizes for read/writing */

#define MAC_OFFSET 0x630


// All MACs must be 98:f0:58:00:00:00 to 98:f0:58:ff:ff:ff

#define MAC_UID1 0x98
#define MAC_UID2 0xf0
#define MAC_UID3 0x58

/* The globally public e2 data structure. It is read only once at startup. */

extern struct e2data onyxxConfig;

typedef enum {
	LLEVEL_MIN 	  = 1,
	LLEVEL_CURRENT = 1,
	LLEVEL_CB      = 2,
	LLEVEL_MAX
} loglevel_e;

typedef enum {
	PING_MIN	= 1,
	PING_DEFAULT = 1,
	PING_USB	= 2,
	PING_WIRELESS = 3,
	PING_ETH	= 4,
	PING_MAX
} pinginterface_e;

typedef enum {
	LVIEW_MIN 	= 1,
	LVIEW_WHOLE	= 1,
	LVIEW_TAIL 	= 2,
	LVIEW_MAX
} logview_e;

typedef enum {
	LTYPE_MIN 	= 1,
	LTYPE_SYS 	= 1,
	LTYPE_DAEMON= 2,
	LTYPE_MSG	= 3,
	LTYPE_WTMP	= 4,
	LTYPE_AUTH	= 5,
	LTYPE_DEBUG	= 6,
	LTYPE_KERN 	= 7,
	LTYPE_MAX
} logtype_e;

// Cli based options

typedef enum {
	typLast 		= 0,
	typNone			= 1 << 0,
	typEmpty		= 1 << 1,
	typMenuChoice	= 1 << 2,
	typItemChoice	= 1 << 3,
	typTitle		= 1 << 4,
	typInlineTitle	= 1 << 5,
	typInteger		= 1 << 6,		/* 16 or 32 bit integer */
	typString		= 1 << 7,		/* Octet String */
	typIpAddr		= 1 << 8,		/* IP address xxx.xxx.xxx.xxx */
	typSel			= 1 << 9,
	typMac			= 1 << 10,		/* MAC xx:xx:xx:xx:xx:xx */
	typIntFunc		= 1 << 11,		/* Call back function that returns an int */
	typStrFunc		= 1 << 12,		/* Call back function that returns a string */
	typCmdFunc		= 1 << 13,
	typVar			= 1 << 14,
	typHidden		= 1 << 15		/* Item or menu is hidden/unselectable */
} cliflags_e;

struct clinode_s
{
	char *cliString;
	char cliKey;
	cliflags_e cliFlags;

	union {
		callback_int cb_int; /* Callback to a int returning function */
		callback_str cb_str; /* Callback to a str returning function */
		callback_cmd cb_cmd; /* Callback to a command based function */
		void *cb_var;
		unsigned long nextMenu; /* Index to another menu */
	};
};

struct responseArray {
	char *respString;
	char *interpString;
};

#define MAC_LEN	6

extern unsigned int lsmHWAddress[MAC_LEN];

extern loglevel_e logLevel;
extern logtype_e logType;
extern logview_e logView;
extern pinginterface_e pingInterface;

#endif /* COMMON_H_ */
