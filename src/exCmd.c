/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: exCmd.c
 *
 * About:
 *
 * This file contains miscellaneous support functions for Onyxx-Commander files.
 */

#include "common.h"
#include "exCmd.h"
#include "menuTree.h"

char resultBuf[SIZEOF_RESULT];

int executeCommand(char *cmdstr, char *outbuf)
{
  int retVal = CMD_ERROR; // default failure
  FILE *fp;
  char path[1035];

  /* Open the command for reading. */

  outbuf[0] = 0;
  fp = popen(cmdstr, "r");
  if (fp != NULL) {
	  /* Read the output a line at a time - output it. */

	  outbuf[0] = 0;

	  while (fgets(path, sizeof(path)-1, fp) != NULL)
	  {
		  strcat(outbuf, path);
		  strcat(outbuf, "\n");
	  }

  /* close */
	  pclose(fp);
	  retVal = CMD_SUCCESS;
  }

  return retVal;
}

void collectProductInfo()
{
	char *s;
	char *e;
	FILE *fp = NULL;

	/* Erase existing info */

	/* Get IP Addresses */

	if (executeCommand("/sbin/ip -o addr 2>/dev/null", resultBuf) == CMD_SUCCESS)
	{
		/* Get eth0 IP */

		s = resultBuf;

		while((s = strstr(s, "eth0 ")) != NULL)
		{
			if ((e = strstr(s, "inet ")) != NULL)
			{
				if ((s = strchr(e, ' ')) != NULL)
				{
					e = strchr(s + 1, ' ') - 1;
					strncpy(productETH0IPV4, s + 1, (e - s));
					productETH0IPV4[(e - s)] = 0;
				}
			} else if ((e = strstr(s, "inet6 ")) != NULL) {
				if ((s = strchr(e, ' ')) != NULL)
				{
					e = strchr(s + 1, ' ') - 1;
					strncpy(productETH0IPV6, s + 1, (e - s));
					productETH0IPV6[(e - s)] = 0;
				}
			}
		}

		/* Get usb0 IP */

		s = resultBuf;
		if ((s = strstr(s, "usb0 ")) != NULL)
		{
			if ((s = strstr(s, "inet ")) != NULL)
			{
				if ((s = strchr(s, ' ')) != NULL)
				{
					e = strchr(s + 1, ' ') - 1;
					strncpy(productUSB0IPV4, s + 1, (e - s));
				}
			}
		}

		/* Get ppp0 IP */

		s = resultBuf;
		if ((s = strstr(s, "ppp0 ")) != NULL)
		{
			if ((s = strstr(s, "inet ")) != NULL)
			{
				if ((s = strchr(s, ' ')) != NULL)
				{
					e = strchr(s + 1, ' ') - 1;
					strncpy(productPPP0IPV4, s + 1, (e - s));
					productPPP0IPV4[(e - s)] = 0;
				}
			}
		}

		/* Get ppp0 Peer IP */

		s = resultBuf;
		if ((s = strstr(s, "ppp0 ")) != NULL)
		{
			if ((s = strstr(s, "peer ")) != NULL)
			{
				if ((s = strchr(s, ' ')) != NULL)
				{
					e = strchr(s + 1, ' ') - 1;
					strncpy(productPPP0Peer, s + 1, (e - s));
					productPPP0Peer[(e - s)] = 0;
				}
			}
		}

	}

	/* Get MAC Numbers */

	if (executeCommand("/sbin/ip -o link 2>/dev/null", resultBuf) == CMD_SUCCESS)
	{
		/* Get eth0 MAC */

		s = resultBuf;
		if ((s = strstr(s, "eth0:")) != NULL)
		{
			if ((s = strstr(s, "link/ether")) != NULL)
			{
				if ((s = strchr(s, ' ')) != NULL)
					strncpy(productETH0MAC, s + 1, 17);

				productETH0MAC[17] = 0;
			}
		}

		s = resultBuf;
		if ((s = strstr(s, "usb0:")) != NULL)
		{
			if ((s = strstr(s, "link/ether")) != NULL)
			{
				if ((s = strchr(s, ' ')) != NULL)
					strncpy(productUSB0MAC, s + 1, 17);

				productUSB0MAC[17] = 0;
			}
		}
	}

    if (fp)
        fclose(fp);
}

/******************************************************
 * Function	   : changeInterfaces
 * Input	   : Ptr to interface string (ie, "iface eth0")
 * 			     Ptr to entry string of interface (ie, "address")
 *               Ptr to replacement parameter for entry string
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Finds the interface entry in the interfaces file
 *               and replaces given newval in the subentry of
 *               the interface.
 *
 *               Function also creates a backup of interfaces file
 *               if one doesn't exist.  Used by factory restore.
 ******************************************************/
int changeInterfaces(char *interf, char *entry, char *newval)
{
	int retVal = CMD_ERROR;
	int found = 0;
	FILE *fpin = NULL;
	FILE *fpout = NULL;
	char *a;
	char *b;
	struct stat fstat;
	char buf[255];

	/* If no backup of interfaces exist, make one here which will be reused in factory reset. */

	if (stat(ORIG_INTERFACE, &fstat) == 0)
	{
		sprintf(buf, "cp /etc/network/interfaces %s 2>/dev/null", ORIG_INTERFACE);
		executeCommand(buf, resultBuf);
	}

	if ((fpin = fopen("/etc/network/interfaces", "r")) != NULL)
	{
		if ((fpout = fopen("/opt/stats/tmp", "w")) != NULL)
		{
			while (!feof(fpin))
			{
				if (fgets(buf, sizeof(buf), fpin))
				{
					if (found)
					{
						if ((a = strstr(buf, entry)))
						{
							b = strchr(a, ' ');

							if (b) {
								// Sub string found, assigned value may exist

								*(b + 1) = 0;
							} else {
								// Sub string found, no currently assigned value

								b = strchr(a, '\n');
								if (b)
									*b = 0;

								strcat(buf, " ");
							}

							strcat(buf, newval);
							strcat(buf, "\n");

							found = 0;
							retVal = CMD_SUCCESS;
						}
					} else {
						if (strstr(buf, interf))
							found = 1;
						else if (strstr(buf, "iface "))
							found = 0;
					}

					if (fwrite(buf, 1, strlen(buf), fpout) != strlen(buf))
						retVal = CMD_ERROR;
				}
			}
		}
	}

	if (fpin)
		fclose(fpin);

	if (fpout)
		fclose(fpout);

	/* Copy tmp over the interface file if we were success file */

	if (retVal == CMD_SUCCESS)
	{
		if (stat("/opt/stats/tmp", &fstat) == 0) {
			executeCommand("cp /opt/stats/tmp /etc/network/interfaces 2>/dev/null", resultBuf);
		} else
			retVal = CMD_ERROR;
	}

	remove("/opt/stats/tmp");

	return retVal;
}

/******************************************************
 * Function	   : getInterfaces
 * Input	   : Ptr to interface string (ie, "iface eth0")
 * 			     Ptr to entry string of interface (ie, "address")
 *               Ptr to receive buffer
 *               Receive buffer length
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Finds the interface entry in the interfaces file
 *               and returns the value associated to the
 *               subentry.
 ******************************************************/
int getInterfaces(char *interf, char *entry, char *bufOut, int bufLen)
{
	int retVal = CMD_ERROR;
	int found = 0;
	FILE *fpin = NULL;
	char *a;
	char *b;
	char buf[255];

	if ((fpin = fopen("/etc/network/interfaces", "r")) != NULL)
	{
		while (!feof(fpin))
		{
			if (fgets(buf, sizeof(buf), fpin))
			{
				if (found)
				{
					if ((a = strstr(buf, entry)))
					{
						b = strchr(a, ' ');

						if (b) {
							// Sub string found, assigned value may exist

							sprintf(bufOut, "%*s", bufLen, b + 1);
							found = 0;
							retVal = CMD_SUCCESS;
						} else {
							// Sub string found but no assigned value

							memset(bufOut, 0, bufLen);
							found = 0;
							retVal = CMD_SUCCESS;
						}
					}
				} else {
					if (strstr(buf, interf))
						found = 1;
					else if (strstr(buf, "iface "))
						found = 0;
				}
			}
		}
	}

	if (fpin)
		fclose(fpin);

	return retVal;
}

/******************************************************
 * Function	   : strwbstr
 * Input	   : Haystack buffer pointer
 *               Stop char
 * 			     Ptr to starting point
 *               Ptr to string to find (needle)
 * Ouput	   : NULL if not found, Ptr of find str otherwise
 * Description : Similar to strstr(), however this function
 *               searches backwards, giving up when we reach
 *               the stop char.  Case sensitive.
 ******************************************************/
char *strbstr(char *haystack, char stopchar, char *starting, char *needle)
{
	char *p = starting;
	char *retVal = NULL;

	while (p && *p != stopchar && *p != 0 && p != haystack)
	{
		if (strncmp(p, needle, strlen(needle)) == 0)
		{
			retVal = p;
			break;
		} else
			p--;
	}

	return retVal;
}

struct termios initial_settings;
struct termios new_settings;

extern int exitflag;

/******************************************************
 * Function	   : cliSetup
 * Input	   : None
 * Ouput	   : None
 * Description : Initializes the terminal to be:
 * 				  non block, echo, no signal ints.
 ******************************************************/
void cliSetup()
{
	memset(&initial_settings, STDIN_FILENO, sizeof(struct termios));
	memset(&new_settings, STDIN_FILENO, sizeof(struct termios));

	tcgetattr(STDIN_FILENO, &initial_settings);
	new_settings = initial_settings;
	new_settings.c_lflag &= ~(ICANON | ECHO | ISIG);
	new_settings.c_cc[VMIN] = 0;
	new_settings.c_cc[VTIME] = 0;

	tcsetattr(0, TCSANOW, &new_settings);
}

/******************************************************
 * Function	   : cliRestore
 * Input	   : None
 * Ouput	   : None
 * Description : Restore terminal settings.  Requires a first
 *  			 call to cliSetup.
 ******************************************************/
void cliRestore()
{
	tcsetattr(STDIN_FILENO, TCSANOW, &initial_settings);
}

/******************************************************
 * Function	   : cliPrint
 * Input	   : string
 * Ouput	   : None
 * Description : Outputs given string to ANSI parser.  String
 *				 does not have to be ANSI, however.
 ******************************************************/
void cliPrint(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	vfprintf(stdout, fmt, ap); fflush(stdout);

	va_end(ap);
}

/******************************************************
 * Function	   : getInput
 * Input	   : Pointer to key collection buffer
 *               Length of buffer
 *               Key required for exiting routine (KYB_KEY_NONE for single key input)
 * Ouput	   : Number of bytes read
 * Description : Gets keyboard input from console.
 ******************************************************/
int getInput(char *keybuf, int len)
{
	int retVal = 0;
	char c;

	fflush(stdin);
	keybuf[0] = 0;

	cliSetup();

	memset(keybuf, 0, len);

	while (1)
	{
		while ((c = getchar()) == EOF);

		if (c == KYB_ESCAPE) // ESCape erases all and aborts
		{
			retVal = 0;
			keybuf[0] = 0;
			break;
		} else if (c == KYB_ENTER) {
			break;
		} else if (c == '@' && exitflag == 0) {
			cliRestore();
			exit(0);
		} else if (c == '\b') {
			if (retVal != 0)
			{
				keybuf[retVal--] = 0;
				cliPrint("\b \b");
			}
		} else {
			if (retVal <= (len - 1))	{
				if (c >= ' ' && c <= '~')
				{
					printf("%c", c);
					keybuf[retVal++] = c;
					keybuf[retVal] = 0;
				}
			} else {
				cliPrint("\b \b"); // Never accept last key, needs to be NULL
				retVal--;
			}
		}
	}

	cliRestore();
	cliPrint("\n");
	return retVal;
}

/******************************************************
 * Function		: strnicmp
 * Input		: char * compare string 1
 *                char * compare string 2
 * Output		: < 0, 0, >0 comparison (0 = equals)
 *
 * Description	: Compares wp strings up to n chars.
 ******************************************************/
int strnicmp (const char *s1, const char *s2, int len)
{
	char c1, c2;
	int v;

	if (len == 0)
		return 0;

	do {
		c1 = *s1++;
		c2 = *s2++;
		/* the casts are necessary when pStr1 is shorter & char is signed */
		v = (unsigned int) toupper(c1) - (unsigned int) toupper(c2);
	} while ((v == 0) && (c1 != '\0') && (--len > 0));

	return v;
}

int stricmp (const char *s1, const char *s2)
{
  while (*s2 != 0 && toupper(*s1) == toupper(*s2))
    s1++, s2++;

  return (int) (toupper(*s1) - toupper(*s2));
}
