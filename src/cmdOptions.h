/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cmdOptions.h
 *
 * About:
 *
 * Support API functions for cmdOptions.c.
 */

#ifndef CMDOPTIONS_H_
#define CMDOPTIONS_H_


#include "common.h"
#include "exCmd.h"

extern int cmdPingIt(void);
extern int cmdShowFsys(void);
extern int cmdChkFsys(void);
extern int cmdFixFsys(void);
extern int cmdProductInfo(void);
extern int cmdChangePassword(void);
extern int cmdResetAll(void);
extern int cmdShowRoutes(void);
extern int cmdCommandRoutes(void);
extern int cmdTraceRoute(void);
extern int cmdReboot(void);
extern int cmdResetDefaults(void);
extern int cmdEraseCode(void);
extern int cmdShowDefMem(void);
extern int cmdSetMAC(void);
extern int cmdSetSN(void);
extern int cmdSetPN(void);
extern int cmdSetCO(void);
extern int cmdDateTime(void);
extern int cmdDumpEEPROM(void);
extern int cmdShowLog(void);
extern int cmdResetLogs(void);
extern int cmdFactoryImage(void);
extern int cmdFactoryOverwrite(void);
extern int cmdSoftwareUpdate(void);

extern int factoryImageReset(void);
extern int getImageRoot(char *dest);
extern int getSoftwareVersion(int *major, int *minor);

#endif /* CMDOPTIONS_H_ */
