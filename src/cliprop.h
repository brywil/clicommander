/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cliprop.h
 *
 * About:
 *
 * Defines the command line properties for the menu engine.
 */

#ifndef CLIPROP_H_
#define CLIPROP_H_

struct cliProp_struct {
	char *cliProp_prompt;
	char *cliProp_leadChar;
	unsigned int cliProp_displayWidth;
	unsigned int cliProp_displayHeight;
	unsigned int cliProp_inactiveTimer;
	unsigned int cliProp_backgroundColor;
	unsigned int cliProp_foregroundColor;
	unsigned int cliProp_promptWindow;
	int currentMenu;
	int currentItem; // For ANSI mode only
	int promptY;
	int ansiMode;
	unsigned char cliProp_autoDetect;
};

#endif /* CLIPROP_H_ */
