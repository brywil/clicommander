/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: strChange.c
 *
 * About:
 *
 * String management and changing functions for the menu engine are handled here.
 */

#include "common.h"
#include "strChange.h"
#include "exCmd.h"

int changeInterfaces(char *interf, char *entry, char *newval);

char networkMask[30];
char networkIp[30];
char adminMask[30];
char adminIp[30];

/******************************************************
 * Function	   : strAdminIP
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of date
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets IP address of USB port.
 ******************************************************/

int strAdminIP(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	FILE *fp;
	char buf[255];
	char *a, *b;

	if (writable)
	{
		if ((fp = fopen("/etc/udhcpd.conf", "r")) != NULL)
		{
			fprintf(fp, "start      %s", newval);
			fprintf(fp, "end        %s", newval);
			fprintf(fp, "interface  usb0");
			fprintf(fp, "max_leases 1");
			fprintf(fp, "option subnet %s", adminMask);
			fclose(fp);

			if ((retVal = changeInterfaces("iface usb0", "address", newval)) != CMD_SUCCESS)
			{
					cliPrint("Cannot change Admin IP At This Time\n");
					retVal = CMD_ERROR;
			}

			if (system("/etc/init.d/udhcpd restart 2>/dev/null") < 0)
				retVal = CMD_ERROR;
			else {
				sleep(4);
				sprintf(buf, "/sbin/ifconfig usb0 %s netmask %s 2>/dev/null", newval, adminMask);

				if (system(buf) < 0)
					retVal = CMD_ERROR;
				else {
					if (system("/usr/sbin/udhcpd -S /etc/udhcpd.conf 2>/dev/null") < 0)
						retVal = CMD_ERROR;
					else
						strcpy(adminIp, newval);
				}
			}
		} else {
			cliPrint("Cannot change Admin IP At This Time\n");
			retVal = CMD_ERROR;
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of USB0 */

		if (executeCommand("ifconfig usb0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "inet addr:")))
			{
				b = strchr(a + 10, ' ');
				*b = 0;
				a = strchr(a, ':') + 1;

				strcpy(newval, (char *) a);
                strcpy(adminIp, newval);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : strAdminMask
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of date
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets IP address of USB port.
 ******************************************************/

int strAdminMask(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	FILE *fp;
	char buf[255];
	char *a, *b;

	if (writable)
	{
		if ((fp = fopen("/etc/udhcpd.conf", "r")) != NULL)
		{
			fprintf(fp, "start      %s", adminIp);
			fprintf(fp, "end        %s", adminIp);
			fprintf(fp, "interface  usb0");
			fprintf(fp, "max_leases 1");
			fprintf(fp, "option subnet %s", newval);
			fclose(fp);

			if ((retVal = changeInterfaces("iface usb0", "netmask", newval)) != CMD_SUCCESS)
			{
					cliPrint("Cannot change Admin mask At This Time\n");
					retVal = CMD_ERROR;
			}

			if (system("/etc/init.d/udhcpd restart 2>/dev/null") < 0)
				retVal = CMD_ERROR;
			else {

				sprintf(buf, "/sbin/ifconfig usb0 %s netmask %s 2>/dev/null", adminIp, newval);
				if (system(buf) < 0)
					retVal = CMD_ERROR;
				else {
					if (system("/usr/sbin/udhcpd -S /etc/udhcpd.conf 2>/dev/null") < 0)
						retVal = CMD_ERROR;
					else
						strcpy(adminMask, newval);
				}
			}
		} else {
			cliPrint("Cannot change USB0 Ip/Mask At This Time\n");
			retVal = CMD_ERROR;
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of USB0 */

		if (executeCommand("ifconfig usb0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "Mask:")))
			{
				b = strchr(a, '\n');
				*b = 0;
				a = strchr(a, ':') + 1;

				strcpy(newval, (char *) a);
                strcpy(adminMask, newval);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : strNetworkIP
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of date
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets IP address of ETH0 port.
 ******************************************************/
int strNetworkIP(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	char buf[255];
	char *a, *b;

	if (writable)
	{
		if ((retVal = changeInterfaces("iface eth0", "address", newval)) == CMD_SUCCESS)
		{
			sprintf(buf, "/sbin/ifconfig eth0 %s netmask %s; ifdown eth0; ifup eth0 2>/dev/null", newval, networkMask);
			if (system(buf) < 0)
				retVal = CMD_ERROR;
			else
				strcpy(networkIp, newval);
		} else {
			cliPrint("Cannot change ETH0 Ip/Mask At This Time\n");
			retVal = CMD_ERROR;
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of ETH0 */

		if (executeCommand("ifconfig eth0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "inet addr:")))
			{
				b = strchr(a + 10, ' ');
				*b = 0;
				a = strchr(a, ':') + 1;

				strcpy(newval, (char *) a);
                strcpy(networkIp, newval);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : strNetworkMask
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of mask
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets mask of ETH0 port.
 ******************************************************/
int strNetworkMask(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	char buf[255];
	char *a, *b;

	if (writable)
	{
		if ((retVal = changeInterfaces("iface eth0", "netmask", newval)) == CMD_SUCCESS)
		{
			sprintf(buf, "/sbin/ifconfig eth0 %s netmask %s; ifdown eth0; ifup eth0 2>/dev/null", networkIp, newval);
			if (system(buf) < 0)
				retVal = CMD_ERROR;
			else
				strcpy(networkMask, newval);
		} else {
			cliPrint("Cannot change ETH0 Ip/Mask At This Time\n");
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of ETH0 */

		if (executeCommand("ifconfig eth0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "Mask:")))
			{
				b = strchr(a, '\n');
				*b = 0;
				a = strchr(a, ':') + 1;

				strcpy(newval, (char *) a);
                strcpy(networkMask, newval);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : strNetworkGateway
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of address
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets gateway IP address of ETH0 port.
 ******************************************************/
int strNetworkGateway(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	char buf[255];
	char *a, *b;

	if (writable)
	{
		if ((retVal = changeInterfaces("iface eth0", "gateway", newval)) == CMD_SUCCESS)
		{
			sprintf(buf, "/sbin/ifconfig eth0 %s netmask %s; ifdown eth0; ifup eth0 2>/dev/null", newval, networkMask);
			if (system(buf) < 0)
				retVal = CMD_ERROR;
		} else {
			cliPrint("Cannot change ETH0 gateway At This Time\n");
			retVal = CMD_ERROR;
		}
	}

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of ETH0 */

		if (executeCommand("ip -4 route list 0/0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "via")))
			{
				b = strchr(a + 4, ' ');
				*b = 0;
				a = strchr(a, ' ') + 1;

				strcpy(newval, (char *) a);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : strWirelessIP
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of IP
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets IP address of PPP0 port.
 ******************************************************/
int strWirelessIP(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	char *a, *b;

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of ETH0 */

		if (executeCommand("ifconfig ppp0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "inet addr:")))
			{
				b = strchr(a + 10, ' ');
				*b = 0;
				a = strchr(a, ':') + 1;

				strcpy(newval, (char *) a);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : strWirelessMask
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of IP
 * Ouput	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets mask of PPP0 port.
 ******************************************************/
int strWirelessMask(unsigned char writable, char *newval)
{
	int retVal = CMD_SUCCESS;
	char *a, *b;

	if (retVal == CMD_SUCCESS)
	{
		/* Get current IP address of ETH0 */

		if (executeCommand("ifconfig ppp0 2>/dev/null", resultBuf) == CMD_SUCCESS)
		{
			/* Return back the IP address */

			if ((a = strstr(resultBuf, "Mask:")))
			{
				b = strchr(a, '\n');
				*b = 0;
				a = strchr(a, ':') + 1;

				strcpy(newval, (char *) a);
				retVal = CMD_SUCCESS;
			} else {
				strcpy(newval, "N//A");
				retVal = CMD_ERROR;
			}
		} else {
			strcpy(newval, "N//A");
			retVal = CMD_ERROR;
		}
	}

	return retVal;
}



