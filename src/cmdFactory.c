/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cmdFactory.c
 *
 * About:
 *
 * This file contains factory restore functions used by the menu engine.
 */

#include "common.h"
#include "exCmd.h"
#include "e2Change.h"
#include "cmdOptions.h"

/******************************************************
 * Function	   : cmdFactoryImage
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Forces factory boot to restore factory
 * image.  Requires a reboot.
 ******************************************************/
int cmdFactoryImage(void)
{
	int retVal = CMD_ERROR;
	char cmd[20];

	cliPrint("\n\n***************************************************\n");
	cliPrint("* WARNING WARNING WARNING WARNING WARNING WARNING *\n");
	cliPrint("*                                                 *\n");
	cliPrint("* This operation will restore device back to      *\n");
	cliPrint("* factory default and will clear all updates.     *\n");
	cliPrint("*                                                 *\n");
	cliPrint("* This operation can take ~20 mins to complete    *\n");
	cliPrint("* and shoult not be interrupted once started!     *\n");
	cliPrint("*                                                 *\n");
	cliPrint("* WARNING WARNING WARNING WARNING WARNING WARNING *\n");
	cliPrint("***************************************************\n");

	cliPrint("\nType 'reset my device' to continue: ");
	memset(cmd, 0, sizeof(cmd));

	if (getInput(cmd, sizeof(cmd) - 1))
	{
		if (strcmp(cmd, "reset my device") != 0)
		{
			cliPrint("\nMagic words not repeated, operation aborted.\n");
		} else {
			cliPrint("\nConfiguring system and rebooting, standby...");

			retVal = factoryImageReset();
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdFactoryImage
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Forces factory boot to restore factory
 * image.  Requires a reboot.
 ******************************************************/
int factoryImageReset(void)
{
	int retVal = CMD_ERROR;
	char cmd[100];
	char mountDir[30];

	/* Get point to factory partition, whether it needs to be mounted or is the boot partition. */

	if (getImageRoot(mountDir) == CMD_SUCCESS)
	{
		/* Backup our stats files to restore later. */

		sprintf(cmd, "cp /opt/stats/.* %sopt/stats", mountDir);
		if (executeCommand(cmd, resultBuf) == CMD_SUCCESS)
		{
			/* Create the "reflash" file on the factory partition. */

			sprintf(cmd, "echo ' ' > %sboot/%s", mountDir, FACTORY_RESET_FILE);
			if (executeCommand(cmd, resultBuf) == CMD_SUCCESS)
			{
				/* Tell the current partition to boot from the factory partition. */

				sprintf(cmd, "cp %sboot/factoryboot.txt %sboot/uEnv.txt", mountDir, mountDir);
				if (executeCommand(cmd, resultBuf) == CMD_SUCCESS)
				{
					cliPrint("\n\n");
					retVal = CMD_SUCCESS; /* for good form */
					strcpy(cmd, "reboot");

					/* There is no returning from this function */

					if (system(cmd) == -1)
						printf("\nERR: Failed to change images.\nPlease call customer service.\nAborting...\n");
				} else {
					cliPrint("\nERR: uEnv not set\nPlease call customer service.\nAborting...\n");
					exit(0);
				}
			} else {
				cliPrint("\nERR: Restore function not set.\nPlease call customer service.\nAborting...\n");
			}
		} else {
			cliPrint("\nERR: Stats move error.\nPlease call customer service.\nAborting...\n");
		}
	} else {
		cliPrint("\nERR: No mount point.\nPlease call customer service.\nAborting...\n");
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdFactoryOverwrite
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Does the actual writing of the factory
 * image over mmcblk0p2.  NO RETURN.
 ******************************************************/
int cmdFactoryOverwrite(void)
{
	int retVal = CMD_ERROR;
	char cmd[100];

	cliPrint("\n\nFACTORY RESET\n\nDO NOT DISTURB UNIT UNTIL COMPLETED!\n\n");

	sprintf(cmd, "gunzip -c /boot/factory.img.gz | pv -s 2400M | dd of=/dev/mmcblk0p2 bs=4M");

	if (executeCommand(cmd, resultBuf) == CMD_SUCCESS)
	{
		/* Successful */

		/* Mount p2 */

		executeCommand("mkdir /mnt/p2", resultBuf);

		if (executeCommand("mount /dev/mmcblk0p2 /mnt/p2", resultBuf) == CMD_SUCCESS)
		{
			executeCommand("cp /opt/stats/.* /mnt/p2/opt/stats", resultBuf);
		}

		cliPrint("\n\nSystem restored.\nRebooting, please wait...");

		/* Remove factory restore file */

		sprintf(cmd, "/boot/%s", FACTORY_RESET_FILE);
		remove(cmd);

		/* Change boot partition back to app partition */

		executeCommand("cp /boot/appboot.txt /boot/uEnv.txt", resultBuf);

		/* Reboot */

		strcpy(cmd, "reboot");

		/* There is no returning from this function */

		if (system(cmd) == -1)
			cliPrint("\nError occured [1]\n");
	}

	return retVal;
}


