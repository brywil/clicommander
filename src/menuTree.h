/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuTree.h
 *
 * About:
 *
 * Definitions and support data for all Onyxx-Commander files.
 */

#ifndef MENUTREE_H_
#define MENUTREE_H_

#define EXIT_ALL				-1
#define MAIN_MENU				0
#define FACTORY_MENU			1
#define SYSTEM_MENU				2
#define TOOLS_MENU				3
#define ROUTE_MENU				4
#define PING_MENU				5
#define FSYS_MENU				6
#define NETWORK_MENU			7
#define LOG_MENU				8
#define LYNX_MENU				9


#define PINGADDR_LEN 		30
#define PRODUCTNAME_LEN 	64
#define PRODUCTIPADDR_LEN 	30
#define PRODUCTMAC_LEN 		30
#define PARTNUMBER_LEN		30

extern char pingAddr[PINGADDR_LEN];
extern char productETH0IPV4[PRODUCTIPADDR_LEN];
extern char productETH0IPV6[PRODUCTIPADDR_LEN];
extern char productUSB0IPV4[PRODUCTIPADDR_LEN];
extern char productPPP0IPV4[PRODUCTIPADDR_LEN];
extern char productPPP0Peer[PRODUCTIPADDR_LEN];
extern char productETH0MAC[PRODUCTMAC_LEN];
extern char productPPP0MAC[PRODUCTMAC_LEN];
extern char productUSB0MAC[PRODUCTMAC_LEN];

extern unsigned long productTTL;

#endif /* MENUTREE_H_ */
