/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: cmdOptions.c
 *
 * About:
 *
 * This file contains support functions for executing numerous menu options and
 * is used by the menu engine as support for the menu tree structure.
 */

#include "common.h"
#include "cmdOptions.h"
#include "e2Change.h"
#include "menuTree.h"

/******************************************************
 * Function	   : strDateTime
 * Input	   : true = write, false = read
 *               Char ptr to output buffer of time
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Gets and/or sets time.
 ******************************************************/

int cmdDateTime(void)
{
	int retVal = CMD_ERROR;
	char outbuf[100];
	char strDate[16];
	char strTime[16];
	int year, mon, day;
	int hour, min, sec;
    struct tm *timeinfo;
    time_t rawtime;

   	time(&rawtime);
   	timeinfo = localtime(&rawtime);

   	cliPrint("Enter new date YYYY-MM-DD (%04d-%02d-%02d): ",
   			timeinfo->tm_year + 1900, timeinfo->tm_mon, timeinfo->tm_mday);

   	if (getInput(strDate, 11) != 0)
	{
		if (sscanf(strDate, "%04d-%02d-%02d", &year, &mon, &day) == 3)
		{
			cliPrint("Enter new 24-hr time HH:MM:SS (%02d:%02d:%02d): ",
					timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);

			if (getInput(strTime, 9) != 0)
			{
				if (sscanf(strTime, "%02d:%02d:%02d", &hour, &min, &sec) == 3)
				{

					/* Write contents back to system as new time */

					sprintf(outbuf, "hwclock --set --date=\"%s %s\" 2>/dev/null", strDate, strTime);
					if (executeCommand(outbuf, resultBuf) == CMD_ERROR)
					{
						cliPrint("Error: Could not set RTC\n");
					} else {
						// Force new time to the RTC

						if (executeCommand("hwclock --hctosys 2>/dev/null", resultBuf) == CMD_ERROR)
						{
							cliPrint("Error: Could not set RTC to System\n");
						} else {
							strcpy(outbuf, "hwclock");
							if (executeCommand(outbuf, resultBuf) == CMD_ERROR)
							{
								cliPrint("Error: Unable to verify RTC date and time.\n");
							} else {
								cliPrint(outbuf, "RTC Time/Date now set to: %s\n", resultBuf);
								retVal = CMD_SUCCESS;
							}
						}
					}
				}
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdPingIt
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Pings the address stored in the pingAddr
 *               variable.  Output is directly placed on
 *               terminal.
 ******************************************************/
int cmdPingIt(void)
{
	int retVal = CMD_SUCCESS;
	char outcmd[200];

	cliPrint("\r\rPinging..\r\r");
	sprintf(outcmd, "ping %s -c 3 %s",
			(pingInterface == PING_DEFAULT) ? "" :
			(pingInterface == PING_USB) ? "-I usb0" :
			(pingInterface == PING_WIRELESS) ? "-I ppp0" :
			(pingInterface == PING_ETH) ? "-I eth0" : "",
			pingAddr);
	retVal = system(outcmd);

	return retVal;
}

/******************************************************
 * Function	   : cmdShowFsys
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Display file block info for file system.
 ******************************************************/
int cmdShowFsys(void)
{
	int retVal = CMD_SUCCESS;

	cliPrint("\r\r");
	retVal = system("df \\.");

	return retVal;
}

/******************************************************
 * Function	   : cmdChkFsys
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Display chk errors and status.
 ******************************************************/
int cmdChkFsys(void)
{
	int retVal = CMD_SUCCESS;

	cliPrint("\r\r");
	retVal = system("fsck.ext4 -n -f -t /dev/mmcblk0p1");

	return retVal;
}

/******************************************************
 * Function	   : cmdFixFsys
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Sets filesys for check on next boot.
 ******************************************************/
int cmdFixFsys(void)
{
	int retVal = CMD_SUCCESS;
	char linebuf[4];
	int v;

	cliPrint("\r\r");
	cliPrint("Unit must reboot to complete operation.\n");
	cliPrint("Reboot? (Y/N): ");

	if (getInput(linebuf, 2))
	{
		if (linebuf[0] == 'y' || linebuf[0] == 'Y')
		{
			cliPrint("\nPreparing...");
			sleep(3);
			if ((v = executeCommand("echo y > /forcefsck", resultBuf)) == CMD_SUCCESS)
			{
				cliPrint("Booting, please wait.\n\n");
				cmdReboot();
			} else {
				cliPrint("Unable to complete.\n");
				retVal = CMD_ERROR;
			}
		}
	}

	printf("\n");

	return retVal;
}

/******************************************************
 * Function	   : cmdProductInfo
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Loads local variables with product info
 *               to be seen on ABOUT menu.
 ******************************************************/
int cmdProductInfo(void)
{
	int retVal = CMD_SUCCESS;
	struct statdata s;

	collectProductInfo();

	cliPrint("\n\nProduct Information:\n");
	cliPrint("S/N: %s\n", onyxxConfig.serial_number);
	cliPrint("eth0: IPv4: %s MAC: %s\n", productETH0IPV4, productETH0MAC);

	if (strlen(productETH0IPV6) > 0)
	{
		cliPrint("eth0: IPv6: %s MAC: %s\n", productETH0IPV6, productETH0MAC);
	}

	if (strlen(productUSB0IPV4) > 0)
	{
		cliPrint("usb0: IPv4: %s MAC: %s\n", productUSB0IPV4, productUSB0MAC);
	}

	if (strlen(productPPP0IPV4) > 0)
	{
		cliPrint("ppp0: IPv4: %s Peer:%s\n", productPPP0IPV4, productPPP0Peer);
	}

	cliPrint("Revision Info: 0x%04X\n", onyxxConfig.version);

	if (readStats(&s) == CMD_SUCCESS)
	{
		/* -------------------------------- Partition Statistical Data Items */

		cliPrint("\nTTL: %lu", s.ttl);
		cliPrint("\nReboots: %i", s.num_reboots);
		cliPrint("\nCritical Boots: %i", s.num_criticals);
		cliPrint("\nLogins: %i", s.num_admin_logins);
		cliPrint("\nUpgrade Count: %i", s.num_upgrades);
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdChangePassword
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Allows user to change ROOT password while
 *               adhering to password format rules.
 ******************************************************/
int cmdChangePassword(void)
{
	int retVal = CMD_ERROR;
	int i;
	char pwd1[DEF_PASSWORD_LEN];
	char pwd2[DEF_PASSWORD_LEN];
	char outbuf[100];
	int rule_uc, rule_lc, rule_punct, rule_digit, rule_space;
	FILE *fp;

	while (retVal == CMD_ERROR)
	{
		memset(pwd1, 0, sizeof(pwd1));
		memset(pwd2, 0, sizeof(pwd2));

		cliPrint("Enter new root password or blank for factory default.\n");
		cliPrint("Password rules:\n");
		cliPrint("\t\t> be 9 to %d characters in length\n", DEF_PASSWORD_LEN - 1);
		cliPrint("\t\t> Contain at least one UPPERCASE letter\n");
		cliPrint("\t\t> Contain at least one lowercase letter\n");
		cliPrint("\t\t> Contain at least one number (0-9)\n");
		cliPrint("\t\t> NOT include any spaces\n");
		cliPrint("\t\t> Contain at least one of the following special characters:\n");
		cliPrint("\t\t\t! \" # % &  ' ( ) ; < = > ?\n");
		cliPrint("\t\t\t[ \\ ] * + , - . / : ^ _ { | } ~\n\n");

		/* First attempt */

		cliPrint("Password: ");
		if (getInput(pwd1, DEF_PASSWORD_LEN - 1) == 0)
		{
			/* Default password ("root"). */

			sprintf(outbuf, "passwd root");
			if ((fp = popen(outbuf, "w")) != NULL)
			{
				sprintf(outbuf, "%s\n", DEFAULT_PASSWORD);
				fprintf(fp, "%s", outbuf);
				fprintf(fp, "%s", outbuf);
				fclose(fp);

				cliPrint("\nPassword reset to factory default.\n");
				retVal = CMD_SUCCESS;

				continue;
			}
		}

		/* Check rules */

		if (strlen(pwd1) < 9 || strlen(pwd1) > DEF_PASSWORD_LEN)
			continue;

		rule_uc = rule_lc = rule_punct = rule_digit = rule_space = 0;

		for (i = 0; i < strlen(pwd1); i++)
		{
			if (isupper(pwd1[i]))
				rule_uc = 1;

			if (islower(pwd1[i]))
				rule_lc = 1;

			if (ispunct(pwd1[i]))
				rule_punct = 1;

			if (isdigit(pwd1[i]))
				rule_digit = 1;

			if (pwd1[i] == ' ')
				rule_space = 1;
		}

		if (rule_uc && rule_lc && rule_punct && rule_digit && !rule_space)
		{
			/* Recheck */

			cliPrint("Re-enter password: ");
			getInput(pwd2, DEF_PASSWORD_LEN - 1);

			if (strcmp(pwd1, pwd2) == 0 && (strlen(pwd1) == strlen(pwd2)))
			{
				/* Password has passed all criteria, change it. */

				//sprintf(outbuf, "passwd root");
				strcpy(outbuf, "ls -al");
				if ((fp = popen(outbuf, "w")) != NULL)
				{
					fprintf(fp, "%s\n", pwd1);
					fprintf(fp, "%s\n", pwd1);
					fclose(fp);

					cliPrint("\nPassword changed successfully.\n");
					retVal = CMD_SUCCESS;
				}
			} else {
				cliPrint("\nPassword does not match, try again...\n");
			}
		} else {
			cliPrint("\nInvalid format, see rules.\n");
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdResetAll
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Allows user to reset all change-able parameters
 *               in the command tree back to default state.
 ******************************************************/
int cmdResetAll(void)
{
	int retVal = CMD_ERROR;
	struct e2data p;
	int v;
	struct stat buf;
	char outbuf[100];
	FILE *fp;

	/* Make sure the user is good with this! */

	cliPrint("\n\n** WARNING **  Reset back to factory defaults.\n");
	cliPrint("** WARNING **  This option cannot be undone.\n\n");

	cliPrint("Proceed? (yes/no): ");
	memset(outbuf, 0, sizeof(outbuf));
	getInput(outbuf, sizeof(outbuf));

	if (strcmp(outbuf, "yes") == 0)
	{
		if (readEEPROMAt((unsigned char *)&p, EEPROM_START_ADDR, sizeof(struct e2data)) == CMD_SUCCESS)
		{
			// Reset password

			strcpy(outbuf, "passwd root");
			if ((fp = popen(outbuf, "w")) != NULL)
			{
				sprintf(outbuf, "%s\n", DEFAULT_PASSWORD);
				fprintf(fp, "%s", outbuf);
				fprintf(fp, "%s", outbuf);
				fclose(fp);

				cliPrint("Password reset to factory default.\n");
			}
		} else {
			cliPrint("Could not reset factory name and password.\n");
		}
		/* Set active/inactive select objects */

		/* Networking enable */

		cliPrint("Enabling network... ");
		v = executeCommand("/sbin/ip link set eth0 up", resultBuf);

		if (v == CMD_SUCCESS)
			cliPrint("enabled\n");
		else
			cliPrint("error detected\n");
		/* Wi-Fi enable */

		cliPrint("Enabling Wi-Fi... ");
		v = executeCommand("/sbin/ip link set ppp0 up", resultBuf);
		if (v == CMD_SUCCESS)
			cliPrint("enabled\n");
		else
			cliPrint("error detected\n");

		/* NTP enable */

		cliPrint("Enabling NTP... ");
		v = executeCommand("timedatectl set-ntp true", resultBuf);
		if (v == CMD_SUCCESS)
			cliPrint("enabled\n");
		else
			cliPrint("error detected\n");

		/* Onyxx RS485 */

		cliPrint("Enabling Onyxx RS485... ");
		if ((v = executeCommand("/bin/systemctl enable onyxx-ser", resultBuf)) == CMD_SUCCESS)
		{
			sleep(3);
			v = executeCommand("/bin/systemctl start onyxx-ser", resultBuf);
		}

		if (v == CMD_SUCCESS)
			cliPrint("enabled\n");
		else
			cliPrint("error detected\n");

		/* Reset route table */

		cliPrint("Reset IPv4 route table... ");

		/* Copy original route table entry over live table. The original route table was
		 * create when route table was changed from original.
		 */

		/* If no table exist, we're already using the default. */

		if (stat(ORIG_ROUTEV4, &buf) == 0)
		{
			sprintf(outbuf, "cp %s /proc/net/route", ORIG_ROUTEV4);
			if (executeCommand(outbuf, resultBuf) == CMD_SUCCESS)
				cliPrint("restored\n");

			sprintf(outbuf, "%s", ORIG_ROUTEV4);
			remove(outbuf);
		} else {
			printf("Nothing to restore.\n");
		}

		/* Reset route table */

		cliPrint("Reset IPv6 route table... ");

		/* Copy original route table entry over live table. The original route table was
		 * create when route table was changed from original.
		 */

		/* If no table exist, we're already using the default. */

		if (stat(ORIG_ROUTEV6, &buf) == 0)
		{
			sprintf(outbuf, "cp %s /proc/net/ipv6_route", ORIG_ROUTEV6);
			if (executeCommand(outbuf, resultBuf) == CMD_SUCCESS)
				cliPrint("restored\n");

			sprintf(outbuf, "%s", ORIG_ROUTEV6);
			remove(outbuf);
		} else {
			printf("Nothing to restore.\n");
		}

		/* Restoring IP and Network Settings.  */

		cliPrint("Restoring Network Interfaces - YOU MAY LOSE CONNECTION... \n");

		if (stat(ORIG_INTERFACE, &buf) == 0)
		{
			sprintf(outbuf, "cp %s /etc/network/interfaces", ORIG_INTERFACE);
			if (executeCommand(outbuf, resultBuf) == CMD_SUCCESS)
				cliPrint("restored\n");

			sprintf(outbuf, "%s", ORIG_INTERFACE);
			remove(outbuf);
		} else {
			printf("Nothing to restore.\n");
		}

		cliPrint("\n\nFactory Default Complete\n\n");
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdShowRoutes
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Displays the current route table.
 ******************************************************/
int cmdShowRoutes(void)
{
	int retVal = CMD_SUCCESS;

	cliPrint("\n\n---- ROUTE TABLE ----\n\n");

	/* Route table backed up.  Show the table. */

	if (system("route -n") != 0)
		retVal = CMD_ERROR;
	else {
		cliPrint("\nRoute table unavailable\n");
		retVal = CMD_ERROR;
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdCommandRoute
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Allows user to manipulate route table.
 ******************************************************/
int cmdCommandRoutes(void)
{
	int retVal = CMD_SUCCESS;
	char routestr[128];
	char cmdstr[140];
	struct stat buf;

	cliPrint("Enter route table command string where,\n");
	cliPrint("[-v]  [-A  family]  add  [-net|-host]  target [netmask Nm] [gw Gw] [metric N] ...\n");
	cliPrint("    [mss M] [window W] [irtt I] [reject] [mod] [dyn] [reinstate] [[dev] If]\n");

	cliPrint("route ");
	if (getInput(routestr, sizeof(routestr)) == 0)
	{
		/* Back up route tables if a backup doesn't already exist. */

		if (stat(ORIG_ROUTEV6, &buf) == 0)
		{
			sprintf(cmdstr, "cp /proc/net/ipv6_route %s", ORIG_ROUTEV6);
			executeCommand(cmdstr, resultBuf);
		}

		if (stat(ORIG_ROUTEV4, &buf) == 0)
		{
			sprintf(cmdstr, "cp /proc/net/route %s", ORIG_ROUTEV4);
			executeCommand(cmdstr, resultBuf);
		}

		sprintf(cmdstr, "route %s", routestr);
		if (system(cmdstr) != 0)
			retVal = CMD_ERROR;
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdTraceRoute
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Allows user to manipulate route table.
 ******************************************************/
int cmdTraceRoute(void)
{
	int retVal = CMD_SUCCESS;
	char routestr[256];
	char cmdstr[300];

	while (1)
	{
		cliPrint("\nEnter host IP to trace (blank to exit): ");
		memset(routestr, 0, sizeof(routestr));
		if (getInput(routestr, sizeof(routestr)) == 0)
			break;

		sprintf(cmdstr, "traceroute %s", routestr);
		if (system(cmdstr) == -1)
			cliPrint("\nNo Trace\n");
	}

	return retVal;
}

/******************************************************
 * Function	   : cmdReboot
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Allows user to reboot system.
 ******************************************************/
int cmdReboot(void)
{
    char cmdstr[] = "reboot";

    /* There is no returning from this function */

	if (system(cmdstr) == -1)
		cliPrint("\ERR: Reboot failed.  Please call customer service.\n");

    return CMD_SUCCESS;
}

/******************************************************
 * Function	   : cmdDumpEEPROM
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Displays the first 248 bytes of EEPROM
 * to the terminal for review.
 ******************************************************/

int cmdDumpEEPROM(void)
{
	int retVal = CMD_ERROR;
	int w, i;
	char e2buf[250];
	FILE *fp = NULL;

	cliPrint("\nEEPROM Contents:\n");

	w = 0;

	if ((fp = fopen(e2FILE, "rb")) != NULL)
	{
		fseek(fp, 0, 0);

		if (fread(e2buf, 1, sizeof(e2buf), fp) == sizeof(e2buf))
		{
			for (i = 0; i < 244; i++)
			{
				if (w == 0)
				{
					cliPrint("%04X: ", i);
				}

				cliPrint("0x%02x, ", e2buf[i]);

				if (w++ > 8)
				{
					w = 0;
					cliPrint("\n");
				}
			}

			retVal = CMD_SUCCESS;
			cliPrint("\n");
		} else {
			cliPrint("\nError: Could not read EEPROM\n");
		}
	} else {
		cliPrint("\nError: Could not open EEPROM\n");
	}

	if (fp != NULL)
		fclose(fp);

	return retVal;
}

/******************************************************
 * Function	   : cmdShowLog
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Displays the whole-or-tail portion of the
 * chosen log at the chosen log level.
 ******************************************************/
int cmdShowLog(void)
{
	char cmd[100];
	char lfile[50];

	switch(logLevel)
	{
		case LLEVEL_CURRENT:
			strcpy(lfile, LOG_FILE_DIR);
			break;

		default:
		case LLEVEL_CB:
			strcat(lfile, LOG_FILE_CB);
			break;
	}

	switch(logType)
	{
		case LTYPE_SYS:
			strcat(lfile, "syslog");
			break;

		case LTYPE_DAEMON:
			strcat(lfile, "daemon.log");
			break;

		case LTYPE_MSG:
			strcat(lfile, "messages");
			break;

		case LTYPE_WTMP:
			strcat(lfile, "wtmp");
			break;

		case LTYPE_AUTH:
			strcat(lfile, "auth.log");
			break;

		case LTYPE_DEBUG:
			strcat(lfile, "debug");
			break;

		default:
		case LTYPE_KERN:
			strcat(lfile, "kern.log");
			break;
	}

	/* Now display the log */

	if (logView == LVIEW_TAIL)
		sprintf(cmd, "tail %s", lfile);
	else
		sprintf(cmd, "cat %s", lfile);

	if (system(cmd) == -1)
		cliPrint("\nNo Log\n");

    return CMD_SUCCESS;
}

/******************************************************
 * Function	   : cmdResetLogs
 * Input	   : None
 * Output	   : CMD_SUCCESS | CMD_ERROR
 * Description : Deletes all system and CB logs.
 ******************************************************/
int cmdResetLogs(void)
{
	char cmd[100];

	sprintf(cmd, "rm -rf %s* 2>&1", LOG_FILE_DIR);
	if (system(cmd) == -1)
		cliPrint("ERR: Log Reset Failed [1]\n");

	sprintf(cmd, "rm -rf %s* 2>&1", LOG_FILE_CB);
	if (system(cmd) == -1)
		cliPrint("ERR: Log Reset Failed [2]\n");

	cliPrint("\nLog files have been purged.\n");
	return CMD_SUCCESS;
}

