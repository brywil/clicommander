/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: e2Change.h
 *
 * About:
 *
 * Support file for e2Change.c.  Defines the contents of the E^2 memory.
 */

#ifndef E2CHANGE_H_
#define E2CHANGE_H_

#include "common.h"
#include "exCmd.h"

#define STATSDIR	"/opt/stats/"

#define TTLFILE		".ttl"
#define CBCFILE		".cbc"
#define REBOOTFILE	".rb"
#define UPGRADESFILE ".up"
#define LOGINSFILE	".li"

/* MNF Ship code is programmed into the EEPROM before product is shipped.  This erases
 * the odometer and other stats to default values.
 */

#define MNF_SHIP_CODE ((unsigned long) 0x4c594e58)
#define DEFAULT_PASSWORD "root"

/* Size of fields + 1 for null terminator */

#define BOARD_NAME_LEN 		(32 + 1)
#define MANUFACTURER_LEN 	(16 + 1)
#define VERSION_LEN			(4 + 1)
#define MAC_STR_LEN			(18 + 1)
#define PART_NUMBER_LEN 	(16 + 1)
#define SERIAL_NUMBER_LEN 	(12 + 1)
#define CONFIG_OPTIONS_LEN 	(15 + 1)
#define DEF_USERNAME_LEN 	(32 + 1)
#define DEF_PASSWORD_LEN 	(32 + 1)
#define SANITY_LEN			(5 + 1)

/* This is a special calculated value that draws n worth of random numbers from
 * scatter algorithm.  DO NOT CHANGE IT unless you know what you're doing.  Make
 * sure the range value used will generate enough random numbers that you need (plus 1 since
 * zero is always manually added at the end).
 *
 * Common ranges
 *
 * Range Value	Random Range
 * -----------  ------------
 * 03h			1-3
 * 0ch			1-15
 * 30h			1-63
 * b8h			1-255
 * 110h			1-511
 * 240h			1-1023
 * 500h			1-2047
 * ca0h			1-4095
 * 1b00h		1-8191
 * 3500h		1-16383
 * 6000h		1-32768
 * b400h		1-65535
 * 12000h		1-131071
 */

#define SCATTER_RANGE 0xca0
#define SCATTER 1
#define DESCATTER 0

struct e2data {
    char board_name[BOARD_NAME_LEN];
    u_int32_t version;
    char manufacturer[MANUFACTURER_LEN];
    char part_number[PART_NUMBER_LEN];
    char serial_number[SERIAL_NUMBER_LEN];
    char config_options[CONFIG_OPTIONS_LEN];

    /* Manufacturer ship time/date stamp */

    u_int8_t mnf_hour;     /* Hour */
    u_int8_t mnf_min;      /* Minutes */
    u_int8_t mnf_sec;      /* Seconds */
    u_int8_t mnf_dst;      /* Daylight Savings */
    u_int8_t mnf_month;    /* Month */
    u_int8_t mnf_day;      /* Day */
    u_int16_t mnf_year;    /* Year */

    /* The LynxSpring assigned MAC address.  This address overrides the TI Am335x MAC
     * address assigned by TI.
     */

    u_int8_t mnf_mac[MAC_LEN];

    /* Ship flag is set to 0xff at factory before hardware is shipped.
     * When system sees 0xff, it initializes the ttl to 0 and sets
     * shipflag to 0.
     */

    u_int32_t shipflag;

    unsigned char sanity[SANITY_LEN];
};

struct statdata {

    /* Power odometer */

    unsigned long ttl;

    /* Statistics: */

    unsigned int num_reboots;          /* Number of times unit has been rebooted */
    unsigned int num_criticals;        /* Number of times critical error reboots required */
    unsigned int num_admin_logins;     /* Number of root logins */
    unsigned int num_upgrades;         /* Number of OS/Accessory upgrades */
};

extern int e2EraseCode(void);
extern int e2SetCode(void);
extern int e2ShowDefMem(void);
extern int e2Program(void);
extern int e2ResetStats(void);
extern int e2EraseAll(void);
extern int e2SetDateTime(void);
extern int readEEPROMAt(unsigned char *p, unsigned int startAddr, unsigned int len);
extern int writeEEPROMAt(unsigned char *p, unsigned int startAddr, unsigned int len);
extern int readStats(struct statdata *);
extern int e2Obfuscate(void);
extern int e2Deobfuscate(void);

#endif /* E2CHANGE_H_ */
