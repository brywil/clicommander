/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: tglChange.c
 *
 * About:
 *
 * All toggle based menu items are handled herein and are called by the menu engine.
 */

#include "tglChange.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "common.h"
#include "exCmd.h"

loglevel_e logLevel = LLEVEL_MIN;
logtype_e logType	= LTYPE_MIN;
logview_e logView	= LVIEW_MIN;
pinginterface_e pingInterface = PING_MIN;

/******************************************************
 * Function	   : toggleNetworking
 * Input	   : true = write, false = read
 * Ouput	   : new value of select
 * Description : If writing, will change select to Networking
 * 				 enabled or disabled.  If read, returns state of
 *				 network as enabled or disabled.
 ******************************************************/

int
toggleNetworking(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;
	char *s;
	char *e;

	if (executeCommand("/sbin/ip -o link", resultBuf) == CMD_SUCCESS)
	{
		/* Isolate eth0 */

		s = strstr(resultBuf, "eth0");
		e = strchr(s, '\n');
		*e = 0;

		/* Look for UP or DOWN */

		if (strstr(s, "state UP"))
		{
			retVal = FEATURE_ENABLED; /* Enabled */
		} else if (strstr(s, "state DOWN")) {
			retVal = FEATURE_DISABLED; /* Disabled */
		}

		/* If we are also to change the status, do it here. */

		if (writable)
		{
			if (retVal == FEATURE_ENABLED)
			{
				/* Networking is enabled, disable it here */

				if (executeCommand("/sbin/ip link set eth0 down", resultBuf) == CMD_SUCCESS)
					retVal = FEATURE_DISABLED;

				// Give time for system to catch up

				sleep(5);

			} else {
				/* Networking is disabled, enable it here */

				if (executeCommand("/sbin/ip link set eth0 up", resultBuf) == CMD_SUCCESS)
					retVal = FEATURE_ENABLED;

				// Give time for system to catch up

				sleep(5);
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleNTP
 * Input	   : true = write, false = read
 * Ouput	   : new value of select
 * Description : If writing, will change select to NTP
 * 				 enabled or disabled.  If read, returns state of
 *				 NTP as enabled or disabled.
 ******************************************************/

int
toggleNTP(unsigned char writable)
{
	int retVal = FEATURE_DISABLED; // Default disabled

	if (executeCommand("timedatectl status", resultBuf) == CMD_SUCCESS)
	{
		if (strstr(resultBuf, "NTP enabled: yes"))
			retVal = FEATURE_ENABLED; // Enabled
		else if (strstr(resultBuf, "NTP enabled: no"))
			retVal = FEATURE_DISABLED; // Disabled
	}

	if (writable == TRUE)
	{
		// Change the NTP status

		if (retVal == FEATURE_ENABLED) // It's on, turn it off.
		{
			executeCommand("timedatectl set-ntp false", resultBuf);
			retVal = FEATURE_DISABLED;
		} else {
			executeCommand("timedatectl set-ntp true", resultBuf);
			retVal = FEATURE_ENABLED;
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleWifi
 * Input	   : true = write, false = read
 * Ouput	   : new value of select
 * Description : If writing, will change select to Wifi
 * 				 enabled or disabled.  If read, returns state of
 *				 Wifi as enabled or disabled.
 ******************************************************/

int toggleWifi(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;
	char *s;
	char *e;

	if (executeCommand("/sbin/ip -o link", resultBuf) == CMD_SUCCESS)
	{
		/* Isolate eth0 */

		if ((s = strstr(resultBuf, "ppp0")) != NULL)
		{
			e = strchr(s, '\n');
			*e = 0;

			/* Look for UP or DOWN */

			if (strstr(s, "state UP"))
			{
				retVal = FEATURE_ENABLED; /* Enabled */
			} else if (strstr(s, "state UNKNOWN")) {
				retVal = FEATURE_ENABLED; /* Enabled but not connected */
			} else if (strstr(s, "state DOWN")) {
				retVal = FEATURE_DISABLED; /* Disabled */
			}

			/* If we are also to change the status, do it here. */

			if (writable)
			{
				if (retVal == FEATURE_ENABLED)
				{
					/* Networking is enabled, disable it here */

					if (executeCommand("/sbin/ip link set ppp0 down", resultBuf) == CMD_SUCCESS)
						retVal = FEATURE_DISABLED;

				} else {
					/* Networking is disabled, enable it here */

					if (executeCommand("/sbin/ip link set ppp0 up", resultBuf) == CMD_SUCCESS)
						retVal = FEATURE_ENABLED;
				}
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleOnyxx
 * Input	   : true = write, false = read
 * Ouput	   : new value of select
 * Description : If writing, will change select to Onyxx
 * 				 enabled or disabled.  If read, returns state of
 *				 Onyxx as enabled or disabled.
 ******************************************************/

int toggleOnyxx(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;
	char *s;
	char *e;

	if (executeCommand("/bin/systemctl status onyxx-ser", resultBuf) == CMD_SUCCESS)
	{
		/* Isolate status */

		if ((s = strstr(resultBuf, "Active:")) != NULL)
		{
			e = strchr(s, '\n');
			*e = 0;

			/* Look for UP or DOWN */

			if (strstr(s, " active"))
			{
				retVal = FEATURE_ENABLED; /* Enabled */
			} else if (strstr(s, " inactive")) {
				retVal = FEATURE_DISABLED; /* Disabled */
			}

			/* If we are also to change the status, do it here. */

			if (writable)
			{
				if (retVal == FEATURE_ENABLED)
				{
					/* Networking is enabled, disable it here */

					if (executeCommand("/bin/systemctl disable onyxx-ser", resultBuf) == CMD_SUCCESS)
					{
						sleep(3);
						executeCommand("/bin/systemctl stop onyxx-ser", resultBuf);
						retVal = FEATURE_DISABLED;
					}
				} else {
					/* Networking is disabled, enable it here */

					if (executeCommand("/bin/systemctl enable onyxx-ser", resultBuf) == CMD_SUCCESS)
					{
						sleep(3);
						executeCommand("/bin/systemctl start onyxx-ser", resultBuf);
						retVal = FEATURE_ENABLED;
					}
				}
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleMiniJack
 * Input	   : true = write, false = read
 * Ouput	   : new value of select
 * Description : If writing, will change select to Onyxx
 * 				 enabled or disabled.  If read, returns state of
 *				 Onyxx as enabled or disabled.
 ******************************************************/

int toggleMiniJack(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;
	char linebuf[100];
	FILE *fp = NULL;

	if ((fp = fopen("/etc/modprobe.d/g_multi.conf", "r")) != NULL)
	{
		retVal = FEATURE_DISABLED;
	} else {
		retVal = FEATURE_ENABLED;
	}

	if (fp)
		fclose(fp);

	/* If we are also to change the status, do it here. */

	if (writable)
	{
		if (retVal == FEATURE_ENABLED)
		{
			/* Mini Jack is enabled, disable it here */

			if (executeCommand("/bin/rm -rf /etc/modprobe.d/g_multi.conf", resultBuf) == CMD_SUCCESS)
			{
				retVal = FEATURE_DISABLED;
			}
		} else {
			/* Mini Jack is disabled, enable it here */

			if ((fp = fopen("/etc/modeprobe.d/g_milti.conf", "w")) != NULL)
			{
				strcpy(linebuf, "install g_multi /bin/true");
				fwrite(linebuf, 1, strlen(linebuf), fp);
				fclose(fp);
			} else {
				retVal = FEATURE_DISABLED; // Could not enable
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleLogLevel
 * Input	   : true = write, false = read
 * Output	   : new value of select
 * Description : If writing, will change select to the ping
 * 				 usb, wireless or eth interfaces.
 ******************************************************/
int
toggleInterface(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;

	retVal = pingInterface;

	if (writable)
	{
		pingInterface++;
		if (pingInterface >= PING_MAX)
			pingInterface = PING_MIN;

		retVal = pingInterface;
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleLogLevel
 * Input	   : true = write, false = read
 * Output	   : new value of select
 * Description : If writing, will change select to Log Level of
 * 				 Newest, Middle or Oldest.
 ******************************************************/
int
toggleLogLevel(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;

	retVal = logLevel;

	if (writable)
	{
		logLevel++;
		if (logLevel >= LLEVEL_MAX)
			logLevel = LLEVEL_MIN;

		retVal = logLevel;
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleLogType
 * Input	   : true = write, false = read
 * Output	   : new value of select
 * Description : If writing, will change select to Log Type of
 * 				 sys, daemon, msg, wtmp, auth, debug or kern.
 ******************************************************/
int
toggleLogType(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;

	retVal = logType;

	if (writable)
	{
		logType++;
		if (logType >= LTYPE_MAX)
			logType = LTYPE_MIN;

		retVal = logType;
	}

	return retVal;
}

/******************************************************
 * Function	   : toggleLogView
 * Input	   : true = write, false = read
 * Output	   : new value of select
 * Description : If writing, will change select to Log View of
 * 				 Whole-File or just the tail.
 ******************************************************/
int
toggleLogView(unsigned char writable)
{
	int retVal = FEATURE_DISABLED;

	retVal = logView;

	if (writable)
	{
		logView++;
		if (logView >= LVIEW_MAX)
			logView = LVIEW_MIN;

		retVal = logView;
	}

	return retVal;
}
