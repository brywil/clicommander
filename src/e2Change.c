/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: e2Change.c
 *
 * About:
 *
 * E^2 memory management and functionality is herein.  Also included is the capability of
 * encrypting, decrypting and obfuscating data stored in E^2.
 */

#include "common.h"
#include "e2Change.h"

static void getProcMAC(unsigned char *mac);
static void randomScatter(int scatter, unsigned char *inBuf, unsigned char *outBuf, int len);
static int encryptData(unsigned char *buffer, unsigned char *key, int bufLen, int keyLen);
static int decryptData(unsigned char *buffer, unsigned char *key, int bufLen, int keyLen);

/* Define OBFUSCATE to force compression and obfuscation.
 * Undefine OBFUSCATE to force raw, untouched data for debug purposes.
 */

#define OBFUSCATE

/* Global configuration data */

struct e2data onyxxConfig;
unsigned char sanity_str[] = SANITY_STRING;

/******************************************************
 * Function    : e2SetDateTime
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Sets date & time e2 fields to current
 * system setting.
 ******************************************************/
int e2SetDateTime(void)
{
    int retVal = CMD_SUCCESS;
    struct tm *timeinfo;
    time_t rawtime;

	time(&rawtime);
	timeinfo = localtime(&rawtime);

	onyxxConfig.mnf_hour = timeinfo->tm_hour;
	onyxxConfig.mnf_min = timeinfo->tm_min;
	onyxxConfig.mnf_sec = timeinfo->tm_sec;
	onyxxConfig.mnf_day = timeinfo->tm_mday;
	onyxxConfig.mnf_month = timeinfo->tm_mon;
	onyxxConfig.mnf_year = timeinfo->tm_year + 1900;

	cliPrint("New Time: %02d:%02d:%02d\n", onyxxConfig.mnf_hour, onyxxConfig.mnf_min, onyxxConfig.mnf_sec);
	cliPrint("New Date: %02d\\%02d\\%04d\n", onyxxConfig.mnf_month, onyxxConfig.mnf_day, onyxxConfig.mnf_year);

	retVal = e2Obfuscate();

    return retVal;
}

/******************************************************
 * Function    : e2EraseCode
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Unsets the shipped flag.
 ******************************************************/
int e2EraseCode(void)
{
    int retVal = CMD_SUCCESS;

    onyxxConfig.shipflag = 0;
    retVal = e2Obfuscate();

    return retVal;
}

/******************************************************
 * Function    : e2SetCode
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Sets the shipped flag.
 ******************************************************/
int e2SetCode(void)
{
    int retVal = CMD_SUCCESS;

   	onyxxConfig.shipflag = MNF_SHIP_CODE;
   	retVal = e2Obfuscate();

    return retVal;
}

/******************************************************
 * Function    : e2EraseAll
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Erases all e2 to 0.
 ******************************************************/
int e2EraseAll(void)
{
    int retVal = CMD_SUCCESS;

    memset((void *) &onyxxConfig, 0, sizeof(struct e2data));
    memcpy(onyxxConfig.sanity, sanity_str, SANITY_LEN);

    retVal = e2Obfuscate();

    return retVal;
}

/******************************************************
 * Function    : e2ShowDefMem
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Displays contents of e2.
 ******************************************************/
int e2ShowDefMem(void)
{
    int retVal = CMD_SUCCESS;
    struct statdata s;
    int i;
    int d = 0;

    if (e2Deobfuscate() == CMD_SUCCESS)
    {
    	if (memcmp(onyxxConfig.sanity, sanity_str, SANITY_LEN) == 0)
    	{
			/* -------------------------------- EEPROM Data Items */

			cliPrint("Board Name: %s\n", onyxxConfig.board_name);
			cliPrint("Version: 0x%04X\n", onyxxConfig.version);

			cliPrint("Manufacturer: %s\n", onyxxConfig.manufacturer);
			cliPrint("Part Number: %s\n", onyxxConfig.part_number);
			cliPrint("Serial Number: %s\n", onyxxConfig.serial_number);
			cliPrint("Time: %02d:%02d:%02d\n", onyxxConfig.mnf_hour, onyxxConfig.mnf_min, onyxxConfig.mnf_sec);
			cliPrint("Date: %02d\\%02d\\%04d\n", onyxxConfig.mnf_month, onyxxConfig.mnf_day, onyxxConfig.mnf_year);
			cliPrint("MAC: %02x:%02x:%02x:%02x:%02x:%02x\n",
					onyxxConfig.mnf_mac[0], onyxxConfig.mnf_mac[1], onyxxConfig.mnf_mac[2],
					onyxxConfig.mnf_mac[3], onyxxConfig.mnf_mac[4], onyxxConfig.mnf_mac[5]);

			cliPrint("Ship flag: 0x%08X\n", (unsigned int) onyxxConfig.shipflag);
			cliPrint("\nConfig Options:\n");
			for (i = 0; i < CONFIG_OPTIONS_LEN; i++)
			{
				cliPrint("#%02d: %d\t\t", i, onyxxConfig.config_options[i]);

				if (d++ > 3)
				{
					cliPrint("\n");
					d = 0;
				}
			}
    	} else {
    		cliPrint("\nNO DATA == This device has not been programmed == NO DATA\n");
    	}
    }

    if (readStats(&s) == CMD_SUCCESS)
    {
        /* -------------------------------- Partition Statistical Data Items */

		cliPrint("\nTTL: %lu", s.ttl);
		cliPrint("\nReboots: %i", s.num_reboots);
		cliPrint("\nCritical Boots: %i", s.num_criticals);
		cliPrint("\nLogins: %i", s.num_admin_logins);
		cliPrint("\nUpgrade Count: %i", s.num_upgrades);
    }

    return retVal;
}

/******************************************************
 * Function    : e2Program
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Sets e2 MAC, Product ID, Serial Number and
 * other entries.
 ******************************************************/
int e2Program(void)
{
    int retVal = CMD_ERROR;
    int bn;
    int i;
    char bitnum[4];
    char bitval[4];
    char linebuf[100];
    unsigned int m0, m1, m2, m3, m4, m5;
    struct e2data p;

    m0 = m1 = m2 = m3 = m4 = m5 = 0;
    memcpy((void *) &p, (void *) &onyxxConfig, sizeof(struct e2data));

	/* Board Name */

	cliPrint("Board Name (%s): ", p.board_name);

	if (getInput(linebuf, BOARD_NAME_LEN))
	{
		memcpy(p.board_name, linebuf, BOARD_NAME_LEN);
	}

	/* Version */

	cliPrint("Version (0x%04X): ", p.version);

	if (getInput(linebuf, VERSION_LEN))
	{
		sscanf(linebuf, "%04X", &p.version);
	}

	/* Manufacturer */

	cliPrint("Manufacturer (%s): ", p.manufacturer);

	if (getInput(linebuf, MANUFACTURER_LEN))
	{
		memcpy(p.manufacturer, linebuf, MANUFACTURER_LEN);
	}

	/* Part Number */

	cliPrint("Part Number (%s): ", p.part_number);

	if (getInput(linebuf, PART_NUMBER_LEN))
	{
		memcpy(p.part_number, linebuf, PART_NUMBER_LEN);
	}

	/* Serial Number */

	cliPrint("Serial Number (%s): ", p.serial_number);

	if (getInput(linebuf, SERIAL_NUMBER_LEN))
	{
		memcpy(p.serial_number, linebuf, SERIAL_NUMBER_LEN);
	}

	/* MAC Address */

	cliPrint("New MAC (%02X:%02X:%02X:%02X:%02X:%02X): ",
			p.mnf_mac[0], p.mnf_mac[1], p.mnf_mac[2],
			p.mnf_mac[3], p.mnf_mac[4], p.mnf_mac[5]);

	if (getInput(linebuf, MAC_STR_LEN))
	{
		/* Parse string */

		if (sscanf(linebuf, "%02X:%02X:%02X:%02X:%02X:%02X",
				&m0, &m1, &m2, &m3, &m4, &m5) == 6)
		{
			/* Make sure MAC address in range of LynxSpring allowed macs of:
			 * 98:f0:58:00:00:0 -to- 98:f0:58:ff:ff:ff
			 */

			if (m0 == MAC_UID1 && m1 == MAC_UID2 && m2 == MAC_UID3)
			{
				p.mnf_mac[0] = (unsigned char) MAC_UID1;
				p.mnf_mac[1] = (unsigned char) MAC_UID2;
				p.mnf_mac[2] = (unsigned char) MAC_UID3;
				p.mnf_mac[3] = (unsigned char) m3;
				p.mnf_mac[4] = (unsigned char) m4;
				p.mnf_mac[5] = (unsigned char) m5;
			} else {
				cliPrint("\nMac UID must be 98:f0:58\nMAC not changed.\n");
			}
		}
	}

	/* Options Flags */

	while (1)
	{
		cliPrint("Current Config Settings:\n\n");
		bn = 0; /* Use this to force a carriage return */

		for (i = 0; i < CONFIG_OPTIONS_LEN; i++)
		{
			cliPrint("#%02d = %d\t\t", i, p.config_options[i]);

			if (bn++ > 3)
			{
				cliPrint("\n");
				bn = 0;
			}
		}

		cliPrint("\nEntry # (0-%d) : ", (int) CONFIG_OPTIONS_LEN - 1);

		if (getInput(bitnum, 3))
		{
			bn = atoi(bitnum);
			if (bn >=0 && bn <= (sizeof(p.config_options) - 1))
			{
				cliPrint("New Val (%d): ", (p.config_options[bn]));

				if (getInput(bitval, 2))
					p.config_options[bn] = atoi(bitval);
			}
		} else {
			break;
		}
	}

	/* Final */

	cliPrint("\nWrite to EEPROM? (Y/N): ");
	if (getInput(linebuf, 2))
	{
		if (linebuf[0] == 'y' || linebuf[0] == 'Y')
		{
			memcpy((void *) &onyxxConfig, (void *) &p, sizeof(struct e2data));
			memcpy(onyxxConfig.sanity, sanity_str, SANITY_LEN);

			if ((retVal = e2Obfuscate()) == CMD_SUCCESS)
			{
				m0 = (unsigned char) p.mnf_mac[0];
				sprintf(linebuf, "%02X:%02X:%02X:%02X:%02X:%02X",
						MAC_UID1, MAC_UID2, MAC_UID3,
					    p.mnf_mac[3], p.mnf_mac[4], p.mnf_mac[5]);

				if ((retVal = changeInterfaces("iface eth0", "hwaddress", linebuf)) == CMD_ERROR)
					cliPrint("ERROR: MAC Address Programming Failure\n");
				else
					lsmHWAddress[0] = MAC_UID1;
					lsmHWAddress[1] = MAC_UID2;
					lsmHWAddress[2] = MAC_UID3;
					lsmHWAddress[3] = p.mnf_mac[3];
					lsmHWAddress[4] = p.mnf_mac[4];
					lsmHWAddress[5] = p.mnf_mac[5];
			}
		} else {
			cliPrint("Aborting - Nothing written to EEPROM\n");
		}
	}

	return retVal;
}

/******************************************************
 * Function    : e2ResetStats
 * Input       : None
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Erases statistics information.
 ******************************************************/
int e2ResetStats(void)
{
    int retVal = CMD_SUCCESS;
    char buf[100];

    /* Deleting stats files resets them also */

    strcpy(buf, STATSDIR);
    strcat(buf, TTLFILE);
    remove(buf);

    strcpy(buf, STATSDIR);
    strcat(buf, CBCFILE);
    remove(buf);

    strcpy(buf, STATSDIR);
    strcat(buf, REBOOTFILE);
    remove(buf);

    strcpy(buf, STATSDIR);
    strcat(buf, UPGRADESFILE);
    remove(buf);

    strcpy(buf, STATSDIR);
    strcat(buf, LOGINSFILE);
    remove(buf);

    return retVal;
}

/******************************************************
 * Function    : e2Obfuscate
 * Input       : None
 * Output      : CMD_SUCCESS | CMD_ERROR
 * Description : This function obfuscates the e2 memory
 * using a non-repeating random number generator keyed
 * by the device's processor MAC address.  The data is
 * then flung into the e2 memory using random, but
 * reproducible, locations.  This is a security algorithm.
 ******************************************************/
int e2Obfuscate(void)
{
	int retVal = CMD_ERROR;
	unsigned char *outBuf = NULL;
	unsigned char *inBuf = NULL;
    unsigned char mac[6];

    /* Read all of 32K memory into temp buffer */

    if ((inBuf = (unsigned char *) malloc(EEPROM_SIZE)) != NULL)
    {
    	if ((outBuf = (unsigned char *) malloc(EEPROM_SIZE)) != NULL)
    	{
			if (readEEPROMAt(inBuf, EEPROM_START_ADDR, EEPROM_SIZE) == CMD_SUCCESS)
			{
				memcpy(inBuf, (void *) &onyxxConfig, sizeof(onyxxConfig));

				/* Get MAC address from Processor */

				getProcMAC(mac);
#ifdef OBFUSCATE
				/* Encrypt data using MAC address as key */

				encryptData(inBuf, mac, EEPROM_SIZE, sizeof(mac));

				/* Now scatter the data through out EEPROM to obfuscate it. */

				randomScatter(SCATTER, inBuf, outBuf, EEPROM_SIZE);

				/* Write the memory back to EEPROM */

				retVal = writeEEPROMAt(outBuf, EEPROM_START_ADDR, EEPROM_SIZE);
#else
				/* Write the memory back to EEPROM */

				retVal = writeEEPROMAt(inBuf, EEPROM_START_ADDR, EEPROM_SIZE);
#endif

			} else {
				cliPrint("ERR: Could not read EEPROM\n");
			}
    	} else {
    		cliPrint("ERR: Not enough memory to complete operation.\n");
    	}
    } else {
    	cliPrint("ERR: Not enough memory to complete operation.\n");
    }

    if (outBuf != NULL)
    	free(outBuf);

    if (inBuf != NULL)
    	free(inBuf);

    return retVal;
}

/******************************************************
 * Function    : e2Deobfuscate
 * Input       : struct e2data pointer to memory
 * Output      : CMD_SUCCESS | CMD_ERROR
 * Description : This function de-obfuscates the e2 memory
 * using and copies e2data into the given structure ptr.
 ******************************************************/

int e2Deobfuscate()
{
	int retVal = CMD_ERROR;
	unsigned char *outBuf = NULL;
	unsigned char *inBuf = NULL;
    unsigned char mac[6];

	/* Read all of 32K memory into temp buffer */

	if ((inBuf = (unsigned char *) malloc(EEPROM_SIZE)) != NULL)
	{
		if ((outBuf = (unsigned char *) malloc(EEPROM_SIZE)) != NULL)
		{
			if (readEEPROMAt(inBuf, EEPROM_START_ADDR, EEPROM_SIZE) == CMD_SUCCESS)
			{
				/* Get MAC address from Processor */

				getProcMAC(mac);
#ifdef OBFUSCATE
				/* De-scatter data */

				randomScatter(DESCATTER, inBuf, outBuf, EEPROM_SIZE);

				/* Decrypt data using MAC address as key */

				decryptData(outBuf, mac, EEPROM_SIZE, sizeof(mac));

				/* Pull e2data from memory */

				memcpy((void *) &onyxxConfig, outBuf, sizeof(onyxxConfig));
#else
				/* Pull e2data from memory */

				memcpy((void *) &onyxxConfig, inBuf, sizeof(onyxxConfig));
#endif

				retVal = CMD_SUCCESS;
			} else {
				cliPrint("ERR: Could not read EEPROM\n");
			}
		} else {
			cliPrint("ERR: Not enough memory to complete operation.\n");
		}
	} else {
		cliPrint("ERR: Not enough memory to complete operation.\n");
	}

	if (outBuf != NULL)
		free(outBuf);

	if (inBuf != NULL)
		free(inBuf);

	return retVal;
}

/******************************************************
 * Function    : encryptData
 * Input       : void ptr buffer of data to encrpt
 *               uchar to buffer with key
 *               int length of data buffer
 *               int length of key
 * Output      : CMD_SUCCESS | CMD_ERROR
 * Description : This function will encrypted data
 * contained within the specified data buffer using
 * the specified key using AES.
 ******************************************************/
int encryptData(unsigned char *buffer, unsigned char *key, int bufLen, int keyLen)
{
	int retVal = CMD_ERROR;
	char *IV = NULL;
	int blocksize;
	int i;

	MCRYPT td = mcrypt_module_open("rijndael-128", NULL, "cbc", NULL);
	blocksize = mcrypt_enc_get_block_size(td);

	IV = malloc(mcrypt_enc_get_iv_size(td));

	for (i = 0; i < mcrypt_enc_get_iv_size(td); i++)
	{
		IV[i] = lsmHWAddress[i % sizeof(lsmHWAddress)];;
	}

	if (bufLen % blocksize == 0)
	{
		mcrypt_generic_init(td, key, keyLen, IV);
		mcrypt_generic(td, buffer, bufLen);
		mcrypt_generic_deinit (td);
		mcrypt_module_close(td);

		retVal = CMD_SUCCESS;
	}

	return retVal;
}

/******************************************************
 * Function    : decryptData
 * Input       : void ptr buffer of data to encrpt
 *               uchar to buffer with key
 *               int length of data buffer
 *               int length of key
 * Output      : CMD_SUCCESS | CMD_ERROR
 * Description : This function will decrypted data
 * contained within the specified data buffer using
 * the specified key using AES.
 ******************************************************/
int decryptData(unsigned char *buffer, unsigned char *key, int bufLen, int keyLen)
{
	int retVal = CMD_ERROR;
	char *IV = NULL;
	int blocksize;
	int i;

	MCRYPT td = mcrypt_module_open("rijndael-128", NULL, "cbc", NULL);

	if (td != MCRYPT_FAILED)
	{
		blocksize = mcrypt_enc_get_block_size(td);
		IV = malloc(mcrypt_enc_get_iv_size(td));

		if (IV != NULL)
		{
			for (i = 0; i < mcrypt_enc_get_iv_size(td); i++)
			{
				IV[i] = lsmHWAddress[i % sizeof(lsmHWAddress)];
			}

			if (bufLen % blocksize == 0)
			{
				mcrypt_generic_init(td, key, keyLen, IV);
				mdecrypt_generic(td, buffer, bufLen);
				mcrypt_generic_deinit (td);
				mcrypt_module_close(td);
				retVal = CMD_SUCCESS;
			}
		}
	}

	return retVal;
}

/******************************************************
 * Function    : randomScatter
 * Input       : void ptr buffer of data to scatter
 *               int length of data buffer
 * Output      : CMD_SUCCESS | CMD_ERROR
 * Description : This function will use a linear
 * random scatter algorithm to swap bytes within the given
 * buffer.  Running this function on the data buffer a
 * second time will unscatter the same data.
 ******************************************************/
void randomScatter(int scatter, unsigned char *inBuf, unsigned char *outBuf, int len)
{
	unsigned long mask;	/* XOR Mask */
	unsigned long point = 1;
	unsigned int i = 0;

	mask = SCATTER_RANGE; /* Range for numbers */

	/* Now cycle through all sequence elements. */

	do {
		if (point >= 0 && point < len)
		{
			if (scatter == SCATTER)
			{
				outBuf[point] = inBuf[i++];
			} else if (scatter == DESCATTER) {
				outBuf[i++] = inBuf[point];
			}
		}

		/* Compute the next value */

		if (point & 1) {
			/* Shift if low bit is set. */

			point = (point >> 1) ^ mask;
		} else {
			/* XOR if low bit is not set */

			point = (point >> 1);
		}
	} while (point != 1);		/* loop until we return  */

	/* Last point is always 0 which is not calculated in the algorithm */

	if (scatter == SCATTER)
	{
		outBuf[0] = inBuf[i++];
	} else if (scatter == DESCATTER) {
		outBuf[i++] = inBuf[0];
	}
}

/******************************************************
 * Function    : readEEPROM
 * Input       : Pointer to EEPROM structure
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Attempts to read a LynxSpring EEPROM
 * structure from the EEPROM.
 ******************************************************/
int readEEPROMAt(unsigned char *p, unsigned int startAddr, unsigned int len)
{
	int retVal = CMD_ERROR;
	FILE *fp = NULL;

	if ((fp = fopen(e2FILE, "r")) != NULL)
	{
		if (fseek(fp, startAddr, 0) == 0)
		{
			if (fread(p, 1, len, fp) != len)
				cliPrint("Error: Cannot read EEPROM\n");
			else
				retVal = CMD_SUCCESS;
		} else {
			cliPrint("Error: Could not index EEPROM, no data read.");
		}
	} else {
		cliPrint("Error: Cannot open EEPROM\n");
	}

	if (fp)
		fclose(fp);

	return retVal;
}

/******************************************************
 * Function    : writeEEPROM
 * Input       : Pointer to EEPROM structure
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Attempts to write a LynsSpring structure
 * to the EEPROM.
 ******************************************************/

int writeEEPROMAt(unsigned char *p, unsigned int startAddr, unsigned int len)
{
	int retVal = CMD_SUCCESS;
	char cmd[EEPROM_BLK_SIZE];
	FILE *fp = NULL;
	int i;
	int blkcnt = 0;
	unsigned int writeataddr = startAddr;
	int writeatlen = len;

	cliPrint("\nWriting to EEPROM, please wait...\n");

	if ((startAddr < EEPROM_START_ADDR) || (startAddr + len > (EEPROM_SIZE + EEPROM_START_ADDR)))
	{
		cliPrint("\nERR: EEPROM range issue detected (0x%04x, %i).\nAborting...\n", startAddr, len);
		retVal = CMD_ERROR;
	} else {
		if ((fp = fopen(e2FILE, "r+")) != NULL)
		{
			while (writeataddr < (startAddr + len) && retVal == CMD_SUCCESS && writeatlen > 0)
			{
				i = (writeatlen > EEPROM_BLK_SIZE) ? EEPROM_BLK_SIZE : writeatlen;

				cliPrint("Blk %02d of %02d: Writing\r", ++blkcnt, (EEPROM_SIZE / EEPROM_BLK_SIZE));

				if (fseek(fp, writeataddr, 0) == 0)
				{
					if (fwrite(p, 1, i, fp) != i)
					{
						cliPrint("\nERR: Cannot write EEPROM\n");
						retVal = CMD_ERROR;
					} else {
						fflush(fp);
					}
				} else {
					cliPrint("\nERR: Could not address EEPROM.\n");
					retVal = CMD_ERROR;
				}

				if (retVal == CMD_SUCCESS)
				{
					if (fseek(fp, writeataddr, 0) == 0)
					{
						if (fread(cmd, 1, i, fp) != i)
						{
							cliPrint("\nERR: Read failed, EEPROM could be protected.\n");
							retVal = CMD_ERROR;
						} else {
							if (memcmp(p, cmd, i) != 0)
							{
								cliPrint("\nERR: EEPROM is write protected.\n");
								retVal = CMD_ERROR;
							}
						}
					} else {
						cliPrint("\nERR: Could not address EEPROM.\n");
						retVal = CMD_ERROR;
					}
				}

				writeataddr += i;
				writeatlen -= i;
				p += i;
			}
		} else {
			cliPrint("\nERR: Cannot open EEPROM\n");
		}
	}

	if (fp)
		fclose(fp);

	return retVal;
}

/******************************************************
 * Function    : readStats
 * Input       : Pointer to Stats structure
 * Ouput       : CMD_SUCCESS | CMD_ERROR
 * Description : Attempts to read a the Stats file.
 ******************************************************/
int readStats(struct statdata *s)
{
	int retVal = CMD_SUCCESS;
	FILE *fp = NULL;
	char buf[100];
	char val[100];

	/* Default all values to 0 */

	memset(s, 0, sizeof(struct statdata));

	/* Read TTL */

	strcpy(buf, STATSDIR);
	strcat(buf, TTLFILE);

	if ((fp = fopen(buf, "r")) != NULL)
	{
		if (fgets(val, sizeof(val), fp))
		{
			s->ttl = atol(val);
		}

		fclose(fp);
	}

	/* Read CrashBoot (critical resets) */

	strcpy(buf, STATSDIR);
	strcat(buf, CBCFILE);

	if ((fp = fopen(buf, "r")) != NULL)
	{
		if (fgets(val, sizeof(val), fp))
		{
			s->num_criticals = atoi(val);
		}

		fclose(fp);
	}

	/* Read number of reboots (good + bad) */

	strcpy(buf, STATSDIR);
	strcat(buf, REBOOTFILE);

	if ((fp = fopen(buf, "r")) != NULL)
	{
		if (fgets(val, sizeof(val), fp))
		{
			s->num_reboots = atoi(val);
		}

		fclose(fp);
	}

	/* Read number of upgrades */

	strcpy(buf, STATSDIR);
	strcat(buf, UPGRADESFILE);

	if ((fp = fopen(buf, "r")) != NULL)
	{
		if (fgets(val, sizeof(val), fp))
		{
			s->num_upgrades = atoi(val);
		}

		fclose(fp);
	}

	/* Read number of logins -- Actually, the number of times Onyxx-Commander has run... */

	strcpy(buf, STATSDIR);
	strcat(buf, LOGINSFILE);

	if ((fp = fopen(buf, "r")) != NULL)
	{
		if (fgets(val, sizeof(val), fp))
		{
			s->num_admin_logins = atoi(val);
		}

		fclose(fp);
	}

	return retVal;
}

/******************************************************
 * Function    : getProcMAC
 * Input       : Pointer to MAC storage, min 6 bytes
 * Ouput       : None
 * Description : Reads MAC address from processor.
 ******************************************************/
void getProcMAC(unsigned char *mac)
{
     int fd;
     unsigned char *p = NULL;

     fd = open("/dev/mem", O_RDWR | O_SYNC);

     p = (unsigned char *) mmap(0, CTL_MOD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, CTL_MOD_START);

     mac[0] = p[MAC_OFFSET + 4];
     mac[1] = p[MAC_OFFSET + 5];
     mac[2] = p[MAC_OFFSET + 6];
     mac[3] = p[MAC_OFFSET + 7];
     mac[4] = p[MAC_OFFSET + 0];
     mac[5] = p[MAC_OFFSET + 1];

     close(fd);
}
