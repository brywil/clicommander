/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuEngine.c
 *
 * About:
 *
 * Main menu processing is handled herein.
 */

#include "common.h"
#include "menuEngine.h"
#include "cliprop.h"
#include "menuTree.h"
#include "cmdOptions.h"
#include <curses.h>
#include "e2Change.h"

void displayMenu(struct cliProp_struct *wedProps);
void displayStandardMenu(struct cliProp_struct *wedProps);
void displayPrompt(struct cliProp_struct *wedProps);
int selectMenuOption(struct cliProp_struct *wedProps);
int parseCommand(struct cliProp_struct *wedProps, char c);
void standardMenuSelectorEnter(struct cliProp_struct *wedProps, char key);
void menuStack(struct cliProp_struct *wedProps);
int indexToMenu(struct cliProp_struct *wedProps);
char *parseSelect(int index);

extern struct clinode_s menuTree[];

/* Store's the hardware address of the LSM */

unsigned int lsmHWAddress[MAC_LEN];

struct {
	char commandLine[132];
    struct clinode_s *commandNode;
} commandBuffer[40];

int exitflag = 1;

/******************************************************
 * Function	   : main
 * Input	   : None
 * Output	   : None
 * Description : Starts the Onyxx-Commander system.
 ******************************************************/
int main()
{
	struct cliProp_struct view;
    unsigned int i = 0;
    int menuCount = 0;
    char buf[100];
    char val[100];
    FILE *fp = NULL;

    /* Running Onyxx-Commander means a root has logged in.  Increment the login counter here. */

    strcpy(buf, STATSDIR);
    strcat(buf, LOGINSFILE);

    i = 1;

    if ((fp = fopen(buf, "r")) != NULL)
    {
    	if (fgets(val, sizeof(val), fp))
    	{
    		i = atoi(val) + 1;
    	}

    	fclose(fp);
    }

    if ((fp = fopen(buf, "w")) != NULL)
    {
    	fprintf(fp, "%i", i);
    	fclose(fp);
    }

    /* if "/boot/factory_restore" file exists, we are to copy factory image over to mmcblk0p2 as a
     * part of the factory-restore-image process.
     */

    sprintf(buf, "/boot/%s", FACTORY_RESET_FILE);

    if (access(buf, F_OK) == 0)
    {
    	cmdFactoryOverwrite();
    }

    /* If ship flag is set, we cannot exit from this program.  Otherwise, "@" will allow use to exit. */

    memset(&onyxxConfig, 0, sizeof(onyxxConfig));
    memset(lsmHWAddress, 0, sizeof(lsmHWAddress));
    memset(buf, 0, sizeof(buf));

    /* We must load the LynxSpring assigned MAC address to the device in order for Deobfuscation (decryption)
     * to work since the LSM assigned MAC also doubles as the IV of the decryptor-- and the processor
     * MAC works as the key to the decryptor... both are needed.
     */

    if (getInterfaces("iface eth0", "hwaddress", buf, sizeof(buf) - 1) == CMD_SUCCESS)
	{

    	if (sscanf(buf, "%02X:%02X:%02X:%02X:%02X:%02X",
    			&lsmHWAddress[0], &lsmHWAddress[1], &lsmHWAddress[2], &lsmHWAddress[3], &lsmHWAddress[4], &lsmHWAddress[5]) == 6)
		{
			/* Make sure MAC address in range of LynxSpring allowed macs of:
			 * 98:f0:58:00:00:0 -to- 98:f0:58:ff:ff:ff
			 */

			if (lsmHWAddress[0] != MAC_UID1 || lsmHWAddress[1] != MAC_UID2 || lsmHWAddress[2] != MAC_UID3)
			{
				cliPrint("\n\n*** WARNING, MAC ADDRESS NOT ALLOWED ***\n\n");
				memset(lsmHWAddress, 0, sizeof(lsmHWAddress));
			}
		}
	}

    if (e2Deobfuscate() == CMD_SUCCESS)
    {
    	if (onyxxConfig.shipflag == MNF_SHIP_CODE)
    	{
    		exitflag = 1;

    		/* Hide eeprom menu if we're in shipped mode */

    		while (menuCount < LYNX_MENU)
    		{
    			if (menuTree[++i].cliFlags == typTitle)
    				menuCount++;
    		}

    		menuTree[i].cliFlags |= typHidden;
       	} else {
    		cliPrint("******************************\n");
    		cliPrint("*** Shipping Flag Not Set ****\n");
    		cliPrint("*** Exit program with '@' ****\n");
    		cliPrint("******************************\n");

    		exitflag = 0;
    	}
    } //else {
    	//cliPrint("\nCannot read EEPROM.  System exiting...\n");
    	//exit(0);
    //}

	view.cliProp_prompt = "Choice: ";
	view.currentMenu = 0;

	// Start menu system

	menuStack(&view);

	return 0;
}

/******************************************************
 * Function	   : menuStack
 * Input	   : Menu Properties ptr
 * Output	   : None
 * Description : Begins processing of the menu stack as
 *				 defined in the menu tree.
 ******************************************************/
void menuStack(struct cliProp_struct *wedProps)
{
	int exitMode = 0;

	cliPrint("LinkSpring Administration\nwww.lynxspring.com\n\n");

	do {
		displayMenu(wedProps);

		exitMode = selectMenuOption(wedProps);

		/* Allow up and down scroll if in ansi mode. */

	} while (exitMode == false);
}

/******************************************************
 * Function	   : selectMenuOption
 * Input	   : Properties
 * Output	   : True = exit mode select, False = no exit
 * Description : Gets input from user for ANSI or non-ANSI
 *				 modes.
 ******************************************************/
int selectMenuOption(struct cliProp_struct *wedProps)
{
	char c[2];
	int retVal = false;

	/* Get input */

	if (getInput(c, sizeof(c)))
		parseCommand(wedProps, c[0]);

	return retVal;
}

/******************************************************
 * Function	   : parseCommand
 * Input	   : Properties
 *               Parse key from input
 * Output	   : True = exit selected, False = No Exit
 * Description : Parses commands for ANSI mode keyboard
 *				 input.
 ******************************************************/
int parseCommand(struct cliProp_struct *wedProps, char c)
{
	int retVal = 0;

	if (c == 0)
	{
		displayPrompt(wedProps);
	} else {
		standardMenuSelectorEnter(wedProps, (char) toupper(c));
	}

	return retVal;
}

/******************************************************
 * Function	   : indexToMenu
 * Input	   : Properties
 * Ouput	   : Index into menuTree for current menu
 * Description : Returns the index in the menuTree to
 *               the current menu.
 ******************************************************/
int indexToMenu(struct cliProp_struct *wedProps)
{
	int retVal = 0;
	int menuCount = 0;

	while (menuCount < wedProps->currentMenu)
	{
		if (menuTree[++retVal].cliFlags == typTitle)
			menuCount++;
	}

	return retVal;
}

/******************************************************
 * Function	   : standardMenuSelectorEnter
 * Input	   : Properties
 *               Char key of input
 * Ouput	   : None
 * Description : Operates on the select menu item as
 *				 specified by that menu item.
 ******************************************************/
void standardMenuSelectorEnter(struct cliProp_struct *wedProps, char key)
{
	int i = 0;
	int s = 0;
	int v1, v2, v3, v4;
	char newval[100];

	/* Search list to make sure key is legitimate for given menu */

	i = indexToMenu(wedProps);
	while (menuTree[i].cliFlags != typLast)
	{
		if (menuTree[i].cliKey == key && !(menuTree[i].cliFlags & typHidden))
			break;
		else {
			i++;
			s++;
		}
	}

	i = indexToMenu(wedProps) + s;

	if (menuTree[i].cliFlags != typLast)
	{
		switch ((menuTree[i].cliFlags & (typMenuChoice | typSel | typString | typIpAddr | typCmdFunc )))
		{
			case typCmdFunc:
				if (menuTree[i].cb_cmd() == CMD_SUCCESS)
					cliPrint("\nCompleted\n");
				break;

			case typMenuChoice:
				wedProps->currentMenu = menuTree[i].nextMenu;
				break;

			case typSel:

				if (menuTree[i].cliFlags & typIntFunc)
				{
					menuTree[i].cb_int(true);
				}
				break;

			case typString:
				cliPrint("Enter new value (esc to abort): ");
				getInput(newval, sizeof(newval));

				if (menuTree[i].cliFlags & typStrFunc)
				{
					menuTree[i].cb_str(true, newval);
				}
				break;

			case typIpAddr:
				cliPrint("Enter IP address (xxx.xxx.xxx.xxx): ");
				getInput(newval, sizeof(newval));

				sscanf(newval, "%d.%d.%d.%d", &v1, &v2, &v3, &v4);
				if (v1 < 0 || v1 > 255)
					v1 = 0;
				if (v2 < 0 || v2 > 255)
					v2 = 0;
				if (v3 < 0 || v3 > 255)
					v3 = 0;
				if (v4 < 0 || v4 > 255)
					v4 = 0;

				if (menuTree[i].cliFlags & typStrFunc)
					menuTree[i].cb_str(true, newval);
				else
					sprintf((char *) menuTree[i].cb_var, "%d.%d.%d.%d", v1, v2, v3, v4);
				break;
			default:
				break;
		}
	}
}

/******************************************************
 * Function	   : displayPrompt
 * Input	   : Properties
 * Output	   : None
 * Description : Displays prompt in prompt window for
 *				 ANSI, or outputs it for non-ANSI.
 ******************************************************/
void displayPrompt(struct cliProp_struct *wedProps)
{
	cliPrint(wedProps->cliProp_prompt);
}


/******************************************************
 * Function	   : displayMenu
 * Input	   : wedProps
 *				 Command Array
 * Output	   : None
 * Description : Displays menu starting from current global
 *				 and including all immediate children.  Stops
 *				 When reaching next item of the same level as
 *				 the current menu item.
 ******************************************************/
void displayMenu(struct cliProp_struct *wedProps)
{
	displayStandardMenu(wedProps);
	displayPrompt(wedProps);
}

/******************************************************
 * Function	   : displayStandardMenu
 * Input	   : wedProps
 *				 Command Array
 * Output	   : None
 * Description : Displays menu starting from current global
 *				 and including all immediate children.  Displays
 *				 menu in the specific window (top or bottom) as
 *				 dictated by properties.
 ******************************************************/
void displayStandardMenu(struct cliProp_struct *wedProps)
{
	struct clinode_s *node = NULL;
	int index = indexToMenu(wedProps);

	// Clear the area

	cliPrint("\n\n\n");

	while (menuTree[index].cliFlags != typLast) {

		if (menuTree[index].cliFlags & typHidden)
		{
			index++;
			continue;
		}

		node = (struct clinode_s *) &menuTree[index];

		switch((node->cliFlags & (typTitle | typMenuChoice | typSel | typString | typIpAddr | typItemChoice)))
		{
			case typTitle:
				cliPrint("%s\n", node->cliString);
				break;

			case typMenuChoice:
				// This is just an item that is unselectable but shows up in a menu.
				if (node->cliKey != 0)
					cliPrint("%c. %s\n", node->cliKey, node->cliString);
				else
					cliPrint("%s\n", node->cliString);

				break;

			case typSel:
			case typString:
			case typIpAddr:
			case typItemChoice:
				if (node->cliKey != 0)
					cliPrint("%c. %s\n", node->cliKey, parseSelect(index));
				else
					cliPrint("%s\n", parseSelect(index));

				break;

			default:
				if (node->cliKey != 0)
					cliPrint("%c. %s\n", node->cliKey, node->cliString);
				else
					cliPrint("%s\n", node->cliString);

				break;
		}

		index++;
	}
}

/******************************************************
 * Function	   : parseSelect
 * Input	   : menuTree index
 * Output	   : Char pointer to results
 * Description : Interprets the menuTree string into a
 *				 finished string.  Replaces "{" and "}"
 *				 with spaces for items not selected,
 *				 Selects "[" and "]" for item that is
 *				 selected.
 ******************************************************/
char *parseSelect(int index)
{
	int numItems = 0;
	int selItem = 0;
	char *s;
	int i, x;
	char newval[255];
	static char buffer[255];


	/* For call back functions, ask which item should be selected. */

	if (menuTree[index].cliFlags & typIntFunc)
	{
		/* How many index items are there?  Count by counting the number "{". */

		s = menuTree[index].cliString;

		for (i = 0; i < strlen(s); i++)
		{
			if (s[i] == '{')
				numItems++;
		}

		selItem = menuTree[index].cb_int(false); /* false = just reading, not writing! */
		if (selItem > numItems)
			selItem = 0;

		/* Go through the string, replace non selected item braces with spaces.  Selected items
		 * will have brackets replace their braces.
		 */

		strcpy(buffer, s);
		s = buffer;

		x = 0;

		while (s != NULL)
		{
			if ((s = strchr(s, '{')) != NULL)
			{
				x++;

				if (x == selItem)
				{
					*s = '[';

					/* Find closing brace */

					s = (char *) strchr(s, '}');
					*s = ']';
				} else {
					*s = ' ';

					/* Find closing brace */

					s = (char *) strchr(s, '}');
					*s = ' ';
				}
			}
		}
	} else if (menuTree[index].cliFlags & typStrFunc) {
		menuTree[index].cb_str(false, newval);

		sprintf(buffer, menuTree[index].cliString, newval);
	} else if ((menuTree[index].cliFlags & typCmdFunc) && menuTree[index].cliKey == 0) {
		sprintf(buffer, "%s", menuTree[index].cliString);
		menuTree[index].cb_cmd();
	} else if (menuTree[index].cliFlags & typVar) {
		sprintf(buffer, menuTree[index].cliString,(char *) menuTree[index].cb_var);
	} else {
		sprintf(buffer, "%s", menuTree[index].cliString);
	}

	return buffer;
}

