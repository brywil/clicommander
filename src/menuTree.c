/*
 * Program		: Onyxx-Commander
 * Author		: Bryan Wilcutt
 * Date			: 5/26/2016
 * File			: menuTree.c
 *
 * About:
 *
 * This file defines the tree structure used by Onyxx-Commander.  The structure is formatted via several struct
 * elements.
 */

#include <stdlib.h>
#include "common.h"
#include "strChange.h"
#include "menuTree.h"
#include "cmdOptions.h"
#include "e2Change.h"
#include "tglChange.h"

char pingAddr[PINGADDR_LEN] = "0.0.0.0";
char productETH0IPV4[PRODUCTIPADDR_LEN];
char productETH0IPV6[PRODUCTIPADDR_LEN];
char productUSB0IPV4[PRODUCTIPADDR_LEN];
char productPPP0IPV4[PRODUCTIPADDR_LEN];
char productPPP0Peer[PRODUCTIPADDR_LEN];
char productETH0MAC[PRODUCTMAC_LEN];
char productPPP0MAC[PRODUCTMAC_LEN];
char productUSB0MAC[PRODUCTMAC_LEN];

unsigned long productTTL = 0;

struct clinode_s menuTree[] = {
	/* Main Menu #0 */

	{ "Top Menu", 						0,   typTitle, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "System Settings",		   		'1', typMenuChoice, { .nextMenu = SYSTEM_MENU } },
	{ "Tools",							'2', typMenuChoice, { .nextMenu = TOOLS_MENU } },
	{ "Network Settings",				'3', typMenuChoice, { .nextMenu = NETWORK_MENU } },
	{ "Restore Factory Settings", 		'4', typMenuChoice, { .nextMenu = FACTORY_MENU } },
	{ "About",							'?', typItemChoice | typCmdFunc, { .cb_cmd = cmdProductInfo } },
	{ "",								0,   typEmpty, 		{ 0 } },
	{ "Reboot",							'R', typItemChoice | typCmdFunc,{ .cb_cmd = cmdReboot } },
	{ "LynxSpring Setup LSM Menu",		'L', typMenuChoice, { .nextMenu = LYNX_MENU } },
	{ "",								0,   typEmpty, 		{ 0 } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* Factory Reset Menu #1 */

	{ "Factory Menu",					0,   typTitle,      { .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "Set Login Password", 			'1', typItemChoice | typCmdFunc, { .cb_cmd = cmdChangePassword } },
	{ "Restore Factory Defaults",		'2', typItemChoice | typCmdFunc, { .cb_cmd = cmdResetAll } },
	{ "Restore Factory Image",			'3', typItemChoice | typCmdFunc, { .cb_cmd = cmdFactoryImage } },
	{ "Set RTC Date/Time", 				'4', typItemChoice | typCmdFunc, { .cb_cmd = cmdDateTime } },
	{ "Update Onyxx Software",			'5', typItemChoice | typCmdFunc, { .cb_cmd = cmdSoftwareUpdate } },
	{ "", 								0, 	 typMenuChoice, { .cb_var = 0 } },
	{ "Exit to Previous Menu",		   	'X', typMenuChoice, { .nextMenu = MAIN_MENU } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* System Settings Menu #2 */

	{ "System Settings Menu",			0,   typTitle,      { .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "Networking           {ACTIVE}  {INACTIVE}",	'1', typSel | typIntFunc, 	{ .cb_int = toggleNetworking } },
	{ "Wi-Fi                {ACTIVE}  {INACTIVE}",	'2', typSel | typIntFunc, 	{ .cb_int = toggleWifi } },
	{ "NTP                  {ACTIVE}  {INACTIVE}",	'3', typSel | typIntFunc, 	{ .cb_int = toggleNTP } },
	{ "Onyxx RS485          {ENABLED} {DISABLED}",	'4', typSel | typIntFunc, 	{ .cb_int = toggleOnyxx } },
	{ "Mini-jack USB Port	{ENABLED} {DISABLED}",  '5', typHidden | typSel | typIntFunc,   { .cb_int = toggleMiniJack } },
	{ "", 								0, 	 typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",		   	'X', typMenuChoice, { .nextMenu = MAIN_MENU } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* Tools Menu #3 */

	{ "Tools Menu",						0,   typTitle,      { .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "Ping Menu", 						'1', typMenuChoice, { .nextMenu = PING_MENU } },
	{ "fsys Menu", 						'2', typMenuChoice, { .nextMenu = FSYS_MENU } },
	{ "Route Table", 					'3', typMenuChoice, { .nextMenu = ROUTE_MENU } },
	{ "Dump EEPROM",					'4', typItemChoice | typCmdFunc, { .cb_cmd = cmdDumpEEPROM } },
	{ "Log Menu",						'5', typMenuChoice, { .nextMenu = LOG_MENU } },
	{ "", 								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice, { .nextMenu = MAIN_MENU } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* Route Table Entry Menu #4 */

	{ "Router Table",					0,   typTitle,      { .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "\n---- ROUTE TABLE ----\n", 		0,   typItemChoice | typCmdFunc, { .cb_var = cmdShowRoutes } },
	{ "Route Add/Del",					'R', typItemChoice | typCmdFunc, { .cb_cmd = cmdCommandRoutes } },
	{ "Trace Route",					'T', typItemChoice | typCmdFunc, { .cb_cmd = cmdTraceRoute } },
	{ "", 								0,   typEmpty,      { .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice, { .nextMenu = TOOLS_MENU } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* Ping Menu #5 */

	{ "Ping Menu",						0,   typTitle,      { .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "",								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "IP Address           [%s]", 		'1', typIpAddr | typVar, 	{ .cb_var = pingAddr } },
	{ "Interface            {Default}  {USB}  {Wireless}  {ETH}", '2', typSel | typIntFunc, { .cb_int = toggleInterface } },
	{ "Ping It!",						'3', typNone | typCmdFunc, 	{ .cb_cmd = cmdPingIt } },
	{ "", 								0,   typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice, { .nextMenu = TOOLS_MENU } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* FSYS Menu #6 */

	{ "File System Menu",				0,  typTitle,      	{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "Show fsys space", 				'1',typItemChoice | typCmdFunc, { .cb_cmd = cmdShowFsys } },
	{ "Check fsys For Errors",			'2',typItemChoice | typCmdFunc,	{ .cb_cmd = cmdChkFsys } },
	{ "Auto-fix fsys Errors",			'3',typItemChoice | typCmdFunc,	{ .cb_cmd = cmdFixFsys  } },
	{ "", 								0,  typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice,	{ .nextMenu = TOOLS_MENU } },
	{ 0,								0, typLast,			{ 0 } },

	/* Network Menu #7 */

	{ "Network Menu",					0,	typTitle,		{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "Admin USB IP Address       [%s]",  '1',typIpAddr | typStrFunc, { .cb_str = strAdminIP } },
	{ "         Mask              [%s]",  '2',typIpAddr | typStrFunc,	{ .cb_str = strAdminMask } },
	{ "Network ETH IP Address     [%s]",  '3',typIpAddr | typStrFunc,	{ .cb_str = strNetworkIP } },
	{ "         Mask              [%s]",  '4',typIpAddr | typStrFunc, { .cb_str = strNetworkMask } },
	{ "Gateway IP Address         [%s]",  '5',typIpAddr | typStrFunc, { .cb_str = strNetworkGateway } },
	{ "   Wireless IP Address        [%s]",	0,typIpAddr | typStrFunc,	{ .cb_str = strWirelessIP } },
	{ "            Mask              [%s]",  0,typIpAddr | typStrFunc,	{ .cb_str = strWirelessMask } },
	{ "", 								0,  typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice,	{ .nextMenu = MAIN_MENU } },
	{ 0,								0, typLast,			{ 0 } },

	/* Log Menu #8 */

	{ "Log Menu",						0,	typTitle,		{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "Show Logs As...               {Whole}  {Tail}",	                  '1', typSel | typIntFunc,  { .cb_int = toggleLogView } },
	{ "Set Log View Level As...      {System}  {CrashBoot}",               '2', typSel | typIntFunc,  { .cb_int = toggleLogLevel } },
	{ "Log:  {Sys}  {Daemon}  {Msg}  {Wtmp}  {Auth}  {Debug}  {Kern}",  '3', typSel | typIntFunc,  { .cb_int = toggleLogType } },
	{ "Show Log",  							                              '4', typItemChoice | typCmdFunc, { .cb_cmd = cmdShowLog } },
	{ "Reset Logs",										                  'R', typItemChoice | typCmdFunc, { .cb_cmd = cmdResetLogs } },
	{ "", 								0,  typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice,	{ .nextMenu = TOOLS_MENU } },
	{ 0,								0, typLast,			{ 0 } },


    /* Manuf. Setup Menu #9 */

	{ "Manufacture Menu",				0,	typTitle,		{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "",								0,  typEmpty, 	   	{ .cb_var = 0 } },
	{ "Display EEPROM Data",    	    '1',typItemChoice | typCmdFunc,	{ .cb_cmd = e2ShowDefMem } },
	{ "Reset Statistics Data", 	        '2',typItemChoice | typCmdFunc, { .cb_cmd = e2ResetStats } },
	{ "Clear e2",						'3',typItemChoice | typCmdFunc, { .cb_cmd = e2EraseAll } },
	{ "Program e2",   		            '4',typItemChoice | typCmdFunc, { .cb_cmd = e2Program } },
	{ "Erase Shipped Flag", 		    '5',typItemChoice | typCmdFunc,	{ .cb_cmd = e2EraseCode } },
	{ "Set Shipped Flag",   		    '6', typItemChoice | typCmdFunc, { .cb_cmd = e2SetCode } },
	{ "Set Date/Time to current",		'7', typItemChoice | typCmdFunc, { .cb_cmd = e2SetDateTime } },
	{ "", 								0,  typEmpty, 		{ .cb_var = 0 } },
	{ "Exit to Previous Menu",			'X', typMenuChoice,	{ .nextMenu = MAIN_MENU } },
	{ 0,								0, 	 typLast, 		{ 0 } },

	/* Last Entry */
	{ 0,								0, 	 typLast, 		{ 0 } },
};

